module IR = Ir.Nier_cfg

type theory = Real_Theory | Float_Theory | Linear_Real_Theory

exception NotSupposedToHappen of string
let printf = Printf.printf
let ilist_to_string l =
  "["^(string_of_int (List.hd l))^
  (List.fold_left (fun acc e -> acc^", "^(string_of_int e)) "" l)
  ^ "]"
let print_ints l = Printf.printf "%s" (ilist_to_string l)
let print_strs l = List.iter
    (fun s -> Printf.printf "%s," s)
let i64ify = List.map (Int64.of_int)
let intify = List.map (Int64.to_int)
(* let print_i64s l = List.iter (fun x -> printf " %d" (Int64.to_int x)) l *)
let float_of_string_accurate f t=
  match t with
  | Float_Theory -> Printf.sprintf "((_ to_fp 11 53) RNE %.16f))" f
  | Real_Theory | Linear_Real_Theory -> let q = Q.of_float f in
    let num = Q.num q and denum = Q.den q in
    "(/ "^(Z.to_string num)^" "^(Z.to_string denum)^"))"

(* List append tail recursive*)
let latr l1 l2 = List.rev_append (List.rev l1) l2

(* Added underscore such that cells are correctly differentiated *)
let rec stringify_int = function
  | [x] -> "_"^(string_of_int x)
  | x::y -> "_"^(string_of_int x)^(stringify_int y)
  | _ -> failwith "empty list can't be stringfied"
let rec stringify = function
  | [x] -> "_"^Int64.to_string x
  | x::y -> "_"^(Int64.to_string x)^(stringify y)
  | _ -> failwith "empty list can't be stringfied"

(* Header of the SMTLIB2 file *)
let pp_header t =
  let logic_str = match t with
    | Linear_Real_Theory | Real_Theory -> "QF_NRA"
    | Float_Theory -> "QF_FP"
  in
  [";;This file has been generated with onnx2SMTtool";
   "(set-logic "^logic_str^")";"(set-option :produce-models true)"]

(* SMT printing utils *)
(* SMT floating points operations are binary only operators, so extra
 * care must be taken for those*)
let add_fn t = match t with
  | Real_Theory | Linear_Real_Theory -> "+ "
  | Float_Theory -> "fp.add RNE "
let mul_fn t = match t with
  | Linear_Real_Theory | Real_Theory -> "* "
  | Float_Theory -> "fp.mul RNE "
let relu_fun_decl t = match t with
  | Linear_Real_Theory | Real_Theory -> "(define-fun reluR ((ws Real)) Real (ite (> ws 0) ws 0))\n"
  | Float_Theory -> "(define-fun reluFP ((ws Float64)) Float64 (ite (fp.geq ws
  (_ +zero 11 53)) ws (_ +zero 11 53)))\n"
let relu_fun t = match t with
  | Real_Theory | Linear_Real_Theory -> "reluR"
  | Float_Theory -> "reluFP"
let id_fun = "" (*identity is just no operation applied*)
(* let fp_mul_app a b = "("^fp_mul_fun^a^" "^b^")" *)
(* let fp_add_app a b = "("^fp_add_fun^a^" "^b^")" *)
(* let get_assert s = "(assert ("^s^"))" *)

(** [get_flat_indices shape] takes an array of dimensions [shape]
    and returns a (string list) of
    indices (i.e. [3,2,3] -> ["0_0_0","0_0_1","0_0_2"...,"2_1_2"]) *)
(* Added underscores to make cells such as [3,80] and [38,0] printed differents *)
let get_flat_indices shape =
  let res = ref [] in
  let rec flatten acc sh =
    match sh with
    | [] -> res := (acc :: !res)
    | (x :: rest) ->
      loop4int64 acc rest (Int64.pred x)
  and loop4int64 ac sh i =
    if i = Int64.zero then flatten (ac^"_0") sh
    else (
      flatten (ac^"_"^(Int64.to_string i)) sh ;
      loop4int64 ac sh (Int64.pred i))
  in
  flatten "" shape;
  !res

(*[all_coords sh] generates all possible coordinates for a given tensor
 * shape, and returns this as a list of list of ints.
 * ex: [all coords [2;2]] = [[0;0];[0;1];[1;0];[1;1]]*)
let all_coords sh =
  let rec ranges acc sh = match sh with
    | x::y -> ranges ((List.init x (fun i -> i))::acc) y
    | [] -> acc
  (* a list containing a list of all possible indexes, for each dimension *)
  in let xxs = ranges [] sh in
  (* add to each element of the list of all possible coordinates all
   * possible indexes ... *)
  let aux acc xs = List.concat @@
    List.map (fun x -> List.map (fun lt -> x::lt) acc) xs
  (* ... for each dimension, starting from an empty list of
   * possible coordinates *)
  in List.fold_left aux [[]] xxs
let get_flat_indices_cfg shape =
  let coords_int = all_coords shape
  in List.fold_left (fun acc l -> (stringify_int l)::acc) [] coords_int

(** [get_flat_indices_list shape] takes an array of dimensions [shape]
    and returns a (string * i64 list) list of corresponding indices
    indices (i.e. [3,2,3] -> [("0_0_0",[0;0;0]),("2_1_2",[2;1;2])) *)
(* Added underscores to make cells such as [3,80] and [38,0] printed differents *)
let get_flat_indices_list shape =
  let res = ref [] in
  let rec flatten acc sh =
    match sh with
    | [] -> res := (acc :: !res)
    | (x :: rest) ->
      loop4int64 acc rest (Int64.pred x)
  and loop4int64 (acs, acl) sh i =
    if i = Int64.zero then
      flatten (acs^"_0",acl@[i]) sh
    else (
      flatten (acs^"_"^(Int64.to_string i),acl@[i]) sh ;
      loop4int64 (acs, acl) sh (Int64.pred i))
  in
  flatten ("",[]) shape;
  !res

let get_flat_indices_list_cfg shape = match shape with
  | [] -> []
  | sh -> let coords_int = all_coords sh
    in List.fold_left (fun acc l -> (stringify_int l,l)::acc) [] coords_int

let rec is_in_bound indice adim =
  match indice, adim with
  | [],[] -> true
  | i::il, d::dl ->
    if Int64.compare i d < 0 && Int64.compare i Int64.zero >= 0  then
      is_in_bound il dl
    else
      false
  | _, _ -> failwith "smtifyer.is_in_bound"
let rec is_in_bound_cfg indice adim =
  match indice, adim with
  | [],[] -> true
  | i::il, d::dl ->
    if i  < d && i >= 0  then
      is_in_bound_cfg il dl
    else
      false
  | _, _ -> failwith "smtifyer.is_in_bound"


(***************************************************************************)
(* environment utilities *)
(* Environment stores shapes and raw data for all variables.
 * An environment is a list of tuple (string; vdata) representing the name
 * of the variable, its corresponding shape and the (potentially empty)
 * raw data contained*)
(***************************************************************************)

type vdata_cfg = {shape: IR.Vertex.shape ;data : IR.Tensor.t option}
type env_cfg = (string * vdata_cfg) list ref
let env_init_cfg = ref []
let t_name t = match t.IR.Vertex.name with
  | Some n -> n
  | None -> "C_NODE"
let pp_shape sh =
  printf "Printing shape:%!";
  List.iter (fun x -> printf ("%d %!") (Int64.to_int x)) sh;
  printf "\n%!"
let pp_shape_cfg sh =
  printf "Printing shape:%!";
  printf "%s" (ilist_to_string sh);
  printf "\n%!"
let pp_env_cfg env =
  printf "################\n%!";
  printf "Printing environment\n%!";
  List.iter (fun (x,y) -> printf "\nPrint element: \n%!";
              printf "Name %s\n%!" x;
              (* printf "Raw %s\n" y.raw_data; *)
              pp_shape_cfg y.shape) !env
let get_shape_cfg a (env:env_cfg) =
  try (List.assoc a !env).shape
  with Not_found -> failwith ("No shape with name "^a)
let set_shape_cfg a sh env =
  try
    let s = (List.assoc a !env).shape in
    if not (s = sh) then
      failwith ("Shape of "^a^"("^(ilist_to_string sh)^
                ") mismatches the one available ("^
                (ilist_to_string s)^")")
  with Not_found ->
    env := (a, {shape = sh; data = None}) :: !env


(* [get_declarations_cfg env t ] build the string corresponding to the declaration
    of all variables in [env] and their value if necessary
    according to theory t *)
(* Added CELL_ prefix to not bother colibri and other touchy solvers *)
let get_declarations_cfg (env:env_cfg) t =
  let res = ref [] in
  let t_str = match t with
    | Float_Theory -> "Float64"
    | Linear_Real_Theory | Real_Theory -> "Real"
  in
  let rec declare_aux a (d:vdata_cfg) flatenedl =
    let rawd = d.data in
    (* Printf.printf "declaring aux for variable %s\n" a; *)
    (* Printf.printf "shape in env: %s\n" (IR.Tensor.show_shape d.shape); *)
    (* Printf.printf "scanning flatenedl\n"; *)
    match flatenedl with
    | [] -> ()
    | ((x,ind)::ll) ->
      let payload = match rawd with
        | None -> []
        | Some tensor ->
          (* pp_env env; *)
          let pld = IR.Tensor.get_idx tensor ind
          in
          [("(assert (= |CELL_"^a^x^"| "^(float_of_string_accurate pld t)^")")]
      in
      res := ("(declare-fun |CELL_"^a^x^"| () "^t_str^")")::payload@(!res);
      declare_aux a d ll
  in
  (* printf "End of get_declarations\n"; *)
  List.iter
    (fun (a,d) -> declare_aux a d (get_flat_indices_list_cfg d.shape)) !env;
  (* printf "End of get_declarations but for real\n"; *)
  !res

(***************************************************************************)
(* Padding and broadcast utilities *)
(* When computing operations with tensors of different shapes, it is sometimes
 * necessary to perform padding and broadcasting operations.
 * Padding add dimensions to an array on the left or on the right,
 * if necessary.
 *
 * Broadcasting consists on modifying the dimensions of two arrays
 * to obtain compatible dimensions without changing the underlying data.
 * For instance, a matrix of shape [3,2] multiplied (matrix multiplication)
 * with a vector of shape [2] is not a valid operation, but becomes valid
 * if the shape of the vector is [2,1]. The operation will result on a
 * vector of shape [2,1]. For more information,
 * @see <https://github.com/onnx/onnx/blob/master/docs/Broadcasting.md> *)
(***************************************************************************)

(* one_padding [a;b] 3;;
   > [1;1;1;a;b] *)
let rec one_padding l i =
  if i <= 0 then l
  else one_padding (Int64.one::l) (i-1)
let rec one_padding_cfg l i =
  if i <= 0 then l
  else one_padding_cfg (1::l) (i-1)

(* used to make 2D out of 1D according to its position in operation *)
let pad_left_cfg = function
  | [] -> failwith "Impossible to pad empty shape"
  | [a] -> [1;a]
  | x -> x
let pad_right_cfg = function
  | [] -> failwith "Impossible to pad empty shape"
  | [a] -> [a;1]
  | x -> x
let pad_cfg (a:IR.Vertex.shape) (b:IR.Vertex.shape) =
  let adim =
    one_padding_cfg a ((List.length b) - (List.length a)) in
  let bdim =
    one_padding_cfg b ((List.length a) - (List.length b)) in
  (adim,bdim)

(* Expand dims of result according to dims of operands *)
let broadcast_cfg a b =
  let rec broadcast_aux acc ad bd =
    match ad, bd with
    | [],[] -> List.rev acc
    |(a::la),(b::lb) ->
      if (a = b) then
        broadcast_aux (a::acc) la lb
      else if (a = 1) then
        broadcast_aux (b::acc) la lb
      else if (b = 1) then
        broadcast_aux (a::acc) la lb
      else failwith "Broadcasting failed: one discordance"
    | _,_ -> failwith "Broadcasting failed: size not same"
  in
  broadcast_aux [] a b

let rec get_last_2D = function
  | [x;y] -> (x,y)
  | (_::rest) -> get_last_2D rest
  | _ -> failwith "no_last_2D"

let rec truncate_to l = function
  | 0 -> l
  | n -> truncate_to (List.tl l) (n-1)

(* Index utilities *)

(* Each operand dimension is either 1 or the same as the result dimension.
   The cells in operand are "doubled" to match the dimension
   of the other operand.
   filter_idx puts to 1 the indices that are not 1 when needed *)
let rec filter_idx index dim =
  match index, dim with
  | [_;_], _ -> index           (* necessarily called with dim >= 2 *)
  | (i::idx), (d::dim) ->
    if (d == 1) then (0::(filter_idx idx dim))
    else ((i-1)::(filter_idx idx dim)) (* TODO: why (i-1) ?! *)
  | _ -> failwith "something wrong with filtering index"

(* Broadcasting is implicit:
 * this function returns the index of the original variable instead of the
 * broadcasted one *)
(* TODO The two following functions should be merged *)
let get_original_index index dim =
  match dim with
  | [d] ->
    (* if operand is 1D, one of the last 2dimension of result is
       that dimension, the other is 1*)
    let (n,m) = get_last_2D index in
    if (n == 0) && (m < d) then [m]
    else if (n < d) && (m == 0) then [n]
    else failwith "get_original_index weird"
  | _ ->
    (* if not, remove trailing dims and do as above for the rest*)
    let idx2 =
      truncate_to index (List.length index - List.length dim) in
    filter_idx idx2 dim

let get_original_index_add index dim =
  let idx = truncate_to index (List.length index - List.length dim) in
  let rec aux i d acc =
    match i,d with
    | [],[] -> List.rev acc
    | (h::t),(hh::tt) ->
      if h < hh then
        aux t tt (h::acc)
      else
        aux t tt (0::acc)
    | _,_ -> failwith "get_original_index_add weird"
  in
  aux idx dim []

(* unclear how they differ; so, testing difference *)
(* let test_getting_original_index = *)
(*   printf "Testing the two function for getting original index\n"; *)
(*   printf "It goes BroadcastedIdx of Dim is Originally Idx \n"; *)
(*   printf "[0;2;3] [3;5] [2;3] : %s=%s\n" *)
(*     (ilist_to_string (get_original_index [0;2;3] [3;5])) *)
(*     (ilist_to_string (get_original_index_add [0;2;3] [3;5])); *)
(*   printf "[3;0] [5] [5] : %s=%s\n" *)
(*       (ilist_to_string (get_original_index [3;0] [5])) *)
(*       (ilist_to_string (get_original_index_add [3;0] [5])) *)
(* printf "[0;2;3] [3;5] [2;3] : %s=%s\n"
    (ilist_to_string (get_original_index ))
    (ilist_to_string (get_original_index_add )) *)

(* [1;2;3] -> "123" *)
(* added some _ here to make it work with the index separation *)
let idx_of l =
  List.fold_left (fun y x -> y^"_"^(string_of_int x)) "" l

(* For an index in particular, an N-ARY function fname, and a list of operands
   generates the SMT clode e.g.: for
   >getSerie "fp.add" "12" ["a","b","c"]
   gives
   >(fp.add roundTowardZero a12 (fp.add roundTowardZero b12 c12)
   EDIT: probably useless, see remark1 *)
let rec getSerie fname idx = function                (* right_assoc *)
  | [x] -> "|CELL_"^x^idx^"|"
  | (x::l) -> "("^fname^"|CELL_"^x^idx^"| "^(getSerie fname idx l)^")"
  | [] -> raise (NotSupposedToHappen "getSerie called with empty")

(* For nary operators (res = Sum [a,b,c]
 * EDIT: remark1
 * just checked operators of ONNX, apparently Add is only binary.
 * If this is really the case, all this can be simplified. *)
(* Added CELL_ prefix to not bother colibri and other touchy solvers *)

let get_fp_serie_node inNames outName indexList fname=
  List.map
    (fun idx -> "(assert (= |CELL_"^outName^idx^"| "^(getSerie fname idx inNames)^"))")
    indexList

(* Added CELL_ prefix to not bother colibri and other touchy solvers *)
let get_unary_op opname inn outn indexList =
  List.map
    (fun idx -> "(assert (= |CELL_"^outn^idx^"| ("^opname^" |CELL_"^inn^idx^"|)))")
    indexList


let pp_relu_cfg tname n env t g =
  let (pname,size) = match IR.NierCFG.pred_e g n with
    | [(_,x,_)] -> (x, (get_shape_cfg x env))
    | _ -> failwith "More than one input for ReLU"
  in
  set_shape_cfg tname size env;
  get_unary_op (relu_fun t) pname tname (get_flat_indices_cfg size)
(* Apparently Keras puts an identity layer at the end
 * of its imported model, so we need to deal with them
 * as well *)
let pp_id_cfg tname n env g =
  let (sname,size) = match IR.NierCFG.pred_e g n with
    | [(_,a,_)] ->(
        match [a] = n.IR.Vertex.pred with
        | true -> (a,get_shape_cfg a env)
        | false -> failwith "Identity preds are amiss")
    | _ -> failwith "More than one input for Identity"
  in
  set_shape_cfg tname (get_shape_cfg tname env) env;
  get_unary_op id_fun sname tname (get_flat_indices_cfg size)

(* TODO: this is dégueulasse we will change stringify and
 * get_original_index to work on I64 *)
let stringify_int l = stringify (i64ify l)
(* Store in res the string describing the Add operation on matrices a and b *)
(* Added CELL_ prefix to not bother colibri and other touchy solvers *)
let rec pp_add_cell idx a adim b bdim c cdim t res =
  let add_fn_str = add_fn t in
  match cdim with
  | [] -> let index = intify (List.rev idx) in
    res := ("(assert (= |CELL_"^c^(stringify_int index)^"| ("^add_fn_str^" "^
            "|CELL_"^a^(stringify_int (get_original_index_add index adim))^"| "^
            "|CELL_"^b^(stringify_int (get_original_index_add index bdim))^
            "|)))")::!res
  | (h::l) ->
    for k = 0 to (h-1) do
      pp_add_cell ((Int64.of_int k)::idx) a adim b bdim c l t res
    done
let rec pp_add_cell_cfg idx a adim b bdim c cdim t res =
  let add_fn_str = add_fn t in
  match cdim with
  | [] -> let index = List.rev idx in
    res := ("(assert (= |CELL_"^c^(stringify_int index)^"| ("^add_fn_str^" "^
            "|CELL_"^a^(stringify_int (get_original_index_add index adim))^"| "^
            "|CELL_"^b^(stringify_int (get_original_index_add index bdim))^
            "|)))")::!res
  | (h::l) ->
    for k = 0 to (h-1) do
      pp_add_cell_cfg (k::idx) a adim b bdim c l t res
    done

let pp_add_cfg tname n env t g =
  let ((x,y),(xs,ys)) = match IR.NierCFG.pred_e g n with
    | [(_,a,_);(_,b,_)] ->(
        (*order matters for the operations, so an additional
         * check must be done*)
        match [a;b] = n.IR.Vertex.pred with
        | true ->
          ((a, b),
           (get_shape_cfg a env, get_shape_cfg b env))
        | false -> if List.rev [a;b] = n.IR.Vertex.pred
          then
            ((b, a),
             (get_shape_cfg b env, get_shape_cfg a env))
          else failwith "Add preds are amiss")
    | _ -> failwith "ADD should have exactly 2 inputs"
  in
  if xs = ys then (
    set_shape_cfg tname ys env;
    get_fp_serie_node [x;y] tname (get_flat_indices_cfg ys) (add_fn t))
  else (
    let (xp,yp) = pad_cfg xs ys in
    let zp = broadcast_cfg xp yp in
    set_shape_cfg tname zp env;
    (* printf "Shape of resulting vector %s:" tname; print_i64s zp; printf "\n"; *)
    let res = ref [] in
    pp_add_cell_cfg [] x  xs y ys tname zp t res;
    !res
  )

let pp_mul_cfg tname n env t g =
  let (sname,(xs,size)) = match IR.preds g n with
    | [x;y] -> let xname,yname = (t_name x,t_name y)
      in
      ([xname;yname],(get_shape_cfg xname env,get_shape_cfg yname env))
    | _ -> failwith "Mul should have exactly 2 inputs"
  in
  if xs = size then (
    set_shape_cfg tname size env;
    get_fp_serie_node sname tname (get_flat_indices_cfg size) (mul_fn t))
  else failwith "size mismatch in Mul"

(* Matmul utilities*)
let check_matmul_size_cfg adim_origin bdim_origin =
  let adim2 = pad_left_cfg adim_origin in
  let bdim2 = pad_right_cfg bdim_origin in
  let adim =
    one_padding_cfg adim2 ((List.length bdim2) - (List.length adim2)) in
  let bdim =
    one_padding_cfg bdim2 ((List.length adim2) - (List.length bdim2)) in
  let rec infer_csize_cfg acc ad bd =
    match ad, bd with
    | [m;n],[nn;p] ->
      if  (nn = n) then (n, List.append (List.rev acc) [m;p])
      else raise (NotSupposedToHappen "size of matrices not adequate")
    |(a::la),(b::lb) ->
      if (a = b) then
        infer_csize_cfg (a::acc) la lb
      else if (a = 1) then
        infer_csize_cfg (b::acc) la lb
      else if (b = 1) then
        infer_csize_cfg (a::acc) la lb
      else failwith "Checking matmul_size failed: one discordance"
    | _,_ -> failwith "Checking matmul_size failed"
  in
  infer_csize_cfg [] adim bdim

(* Store in res the string describing the MatMul
 * operation on matrices a and b *)
(* Added CELL_ prefix to not bother colibri and other touchy solvers *)
let rec pp_matmul_cell prefix i j a adim b bdim t pivot =
  let add_fn_str = match t with
    | Linear_Real_Theory | Real_Theory -> "+"
    | Float_Theory -> "fp.add RNE" in
  let mul_fn_str = match t with
    | Linear_Real_Theory | Real_Theory -> "*"
    | Float_Theory -> "fp.mul RNE"
  in
  match pivot with
  | 1 ->"("^mul_fn_str^" |CELL_"^a^
        (idx_of (get_original_index (prefix@[i;0]) adim))^
        "| |CELL_"^b^(idx_of (get_original_index (prefix@[0;j]) bdim))^"|)"
  | n ->"("^add_fn_str^
        " ("^mul_fn_str^
        " |CELL_"^a^
        (idx_of (get_original_index (prefix@[i;(n-1)]) adim))^
        "| "^
        "|CELL_"^b^
        (idx_of (get_original_index (prefix@[(n-1);j]) bdim))^
        "|) "^
        (pp_matmul_cell prefix i j a adim b bdim t (n-1))^")"

let rec mat_mul_cell acc_idx_rev cdim a adim b bdim c pivot str t =
  match cdim with
  | [m;p] ->
    let acc_idx = List.rev acc_idx_rev in
    for i = 0 to (m-1) do
      for j = 0 to (p-1) do
        let prefix = (idx_of (acc_idx@[i;j])) in
        str:= ("(assert (= |CELL_"^c^prefix^"| "^
               (pp_matmul_cell acc_idx i j a adim b bdim t pivot)^"))")::!str
      done
    done
  | (h::l) ->
    for k = 0 to (h-1) do
      mat_mul_cell (k::acc_idx_rev) l a adim b bdim c pivot str t
    done
  | _-> failwith "nothing else can happen in mat_mul_cell"

let pp_matmul_cfg c n env t g =
  let (a,b) = match IR.NierCFG.pred_e g n with
    | [(_,x,_);(_,y,_)] ->(
        (*order matters for the operations, so an additional
         * check must be done*)
        match [x;y] = n.IR.Vertex.pred with
        | true -> (x,y)
        | false -> if List.rev [x;y] = n.IR.Vertex.pred
          then (y,x)
          else failwith "MatMul preds are amiss")
    | _ -> failwith "MatMul should have exactly 2 inputs" in
  let adim = (get_shape_cfg a env) in
  let bdim = (get_shape_cfg b env) in
  let (pivot,cdim) = check_matmul_size_cfg adim bdim in
  let result = ref [] in
  let (adimi, bdimi, cdimi, pivoti) =(adim,bdim,cdim,pivot)
  in
  set_shape_cfg c cdim env;
  (* printf "Shape of resulting matrix %s:" c; print_i64s cdim; printf "\n"; *)
  mat_mul_cell [] cdimi a adimi b bdimi c pivoti result t;
  !result

(* Added CELL_ prefix to not bother colibri and other touchy solvers *)
let pp_transpose_default res a shape_res =
  let str = ref [] in
  let rec transpose idx_acc shape =
    match shape with
    | [] ->
      let id_res = idx_of (List.rev idx_acc) in
      let id_source = idx_of idx_acc in
      let assertion =
        "(assert (= |CELL_"^res^id_res^"| |CELL_"^a^id_source^"|))"
      in
      str := assertion::(!str)
    | d::dims ->
      for i = 0 to (d-1) do
        transpose (i::idx_acc) dims
      done
  in
  transpose [] shape_res;
  !str

let inversed_shape n =
  let l = ref [] in
  for i = 0 to n-1 do
    l := i::!l
  done ;
  !l

let permute_idx (index_in : int64 array) (permut_array : int64 array) =
  let res = ref "" in
  let bnd = Array.length index_in in
  assert (bnd > 0);
  for i = bnd - 1 downto 0 do
    try
      res := (Int64.to_string index_in.(Int64.to_int permut_array.(i))) ^ !res
    with Invalid_argument (_)->
      (* res := (Int64.to_string index_in.(i)) ^ !res *)
      failwith ("permute_idx: i is not in 0-"^string_of_int bnd)
  done;
  !res
let permute_idx_cfg (index_in : int array) (permut_array : int array) =
  let res = ref "" in
  let bnd = Array.length index_in in
  assert (bnd > 0);
  for i = bnd - 1 downto 0 do
    try
      res := (string_of_int index_in.(permut_array.(i))) ^ !res
    with Invalid_argument (_)->
      (* res := (Int64.to_string index_in.(i)) ^ !res *)
      failwith ("permute_idx: i is not in 0-"^string_of_int bnd)
  done;
  !res


let perm_shape_cfg adim perm =
  let adima = Array.of_list adim in
  let perma = Array.of_list perm in
  let res = ref [] in
  let bnd = Array.length adima in
  assert (bnd > 0);
  for i = bnd - 1 downto 0 do
    try
      res := (adima.(perma.(i))) :: !res
    with Invalid_argument (_)->
      (* res := (adima.(i)) :: !res *)
      failwith ("perm_shape: i is not in 0-"^
                (string_of_int bnd)^" in "^
                (stringify_int adim)^" with perm= "^
                (stringify_int perm))
  done;
  !res


(* Added CELL_ prefix to not bother colibri and other touchy solvers *)

let pp_transpose_cfg res n env g =
  let str = ref [] in
  let perm = match n.IR.Vertex.operator_parameters with
    | Some (IR.Vertex.Transpose_params shape) -> shape
    | _ -> failwith "Invalid attribute for pp_transpose" in
  let (a,adim) = match IR.preds g n with
    | [x_] -> let x = t_name x_ in
      let sh = get_shape_cfg x env in
      (x, sh)
    | _ -> failwith "Transpose should have exactly 1 input" in
  let cdim = perm_shape_cfg adim perm in
  (* printf "shape cdim = "; *)
  (* pp_shape cdim; *)
  set_shape_cfg res cdim env;
  if (inversed_shape (List.length cdim)) = perm then
    pp_transpose_default res a cdim
  else
    let permarray = Array.of_list perm in
    let rec go_through sh acc =
      match sh with
      | [] ->
        let indx = List.rev acc in
        let array_of_index = Array.of_list indx in
        let assert_eq v w = "(assert (= |CELL_"^res^v^"| |CELL_"^a^w^"|))" in
        str :=
          (assert_eq (permute_idx_cfg array_of_index permarray)
             (stringify_int indx)):: !str
      | e::rest -> loop4int acc rest (e-1)
    and loop4int ac sh i =
      if i = 0 then
        go_through sh (i :: ac)
      else
        (go_through sh (i :: ac);
         loop4int ac sh (i -1))
    in
    go_through adim [];
    !str

let conv_output_axis x bpad epad krn stride dilation  =
  (* [x + ep + bp − k − (k − 1)(d − 1) ]/s + 1
     (X−K+2P)/S+1.
  *)
  let neg = Int64.add krn (Int64.mul (Int64.pred krn) (Int64.pred dilation)) in
  let pos = Int64.add x (Int64.add bpad epad) in
  Int64.succ (Int64.div (Int64.sub pos neg) stride)
let conv_output_axis_cfg x bpad epad krn stride dilation  =
  (* [x + ep + bp − k − (k − 1)(d − 1) ]/s + 1
     (X−K+2P)/S+1.
  *)
  let neg = krn + ((krn -1 ) * (dilation -1)) in
  let pos = x + (bpad + epad) in
  ((pos-neg)/stride) + 1

let rec conv_output_list xdims bpads epads kdims strides dilations =
  (* Should be called on last 2D - BatchS and Channels are the same
     for output, i.e. not dependent on pads, strides, etc. *)
  match xdims, bpads, epads, kdims, strides, dilations with
  | x::lx, bpad::lbpad,epad::lepad,krn::lkrn,stride::lstride,dilation::ldilation
    -> (conv_output_axis x bpad epad krn stride dilation) ::
       (conv_output_list lx lbpad lepad lkrn lstride ldilation)
  | [],[],[],[],[],[] -> []
  | _ -> failwith "in conv_output_list, not the same sizes in the attributes"
let rec conv_output_list_cfg xdims bpads epads kdims strides dilations =
  (* Should be called on last 2D - BatchS and Channels are the same
     for output, i.e. not dependent on pads, strides, etc. *)
  match xdims, bpads, epads, kdims, strides, dilations with
  | x::lx, bpad::lbpad,epad::lepad,krn::lkrn,stride::lstride,dilation::ldilation
    -> (conv_output_axis_cfg x bpad epad krn stride dilation) ::
       (conv_output_list_cfg lx lbpad lepad lkrn lstride ldilation)
  | [],[],[],[],[],[] -> []
  | _ -> failwith "in conv_output_list, not the same sizes in the attributes"

let rec half_split n acc l =
  if n = 0 then
    (acc, l)
  else
    match l with
    | x::ll -> half_split (n-1) (acc@[x]) ll
    | [] -> failwith "not possible in half_split"

let conv_output_shape xdims bpads epads kdims strides dilations =
  conv_output_list xdims bpads epads kdims strides dilations
let conv_output_shape_cfg xdims bpads epads kdims strides dilations =
  conv_output_list_cfg xdims bpads epads kdims strides dilations

let rec gen_list n e acc =
  if n = 0 then
    acc
  else
    gen_list (n-1) e (e::acc)

(* gives back a list of indexed cells from input matrix
    x[ri*si + ki*di - bpi, ....] for i=0 to length(res_idx), and ri is ith
    res_idx, si is ith strides, etc. *)
let gen_cell res_idx kernel_idx strides dilations bpads =
  let rec compute_idx r k s d b acc =
    match r, k, s, d, b with
    | [re], [ke], [se], [de], [be](* , [ee] *) ->
      List.rev ((Int64.sub (Int64.add (Int64.mul re se) (Int64.mul ke de)) (be)) :: acc)
    | re::rl, ke::kl, se::sl, de::dl, be::bl(* , ee::el *) ->
      compute_idx rl kl sl dl bl (* el *)
        (Int64.sub (Int64.add (Int64.mul re se) (Int64.mul ke de)) (be)::acc)
    | _ -> failwith "something gone wrong in smtifyer.compute_idx"
  in
  compute_idx res_idx kernel_idx strides dilations bpads (* epads *) []
let gen_cell_cfg res_idx kernel_idx strides dilations bpads =
  let rec compute_idx r k s d b acc =
    match r, k, s, d, b with
    | [re], [ke], [se], [de], [be](* , [ee] *) ->
      List.rev ((((re * se) + (ke * de)) - (be)) :: acc)
    | re::rl, ke::kl, se::sl, de::dl, be::bl(* , ee::el *) ->
      compute_idx rl kl sl dl bl (* el *)
        (((re * se) + (ke * de)) - (be)::acc)
    | _ -> failwith "something gone wrong in smtifyer.compute_idx"
  in
  compute_idx res_idx kernel_idx strides dilations bpads (* epads *) []

(* serie_operator takes a string representing a binary operation and a list
   of strings representing operands of that operations and generates the string
   represnting the application.
   E.G.: "serie_operator max [a, b, c, d]" gives back "max a (max b (max c d))"*)
(* let rec serie_operator op operands =
 *   (\* let str = ref "" in *\)
 *   match operands with
 *   | [] -> failwith "impossible serie_operator"
 *   | [a] -> a
 *   | a :: l -> op^" "^a^" ("^(serie_operator op l)^")" *)


let pp_maxpool_cfg res n env g =
  (* Currently only applicable to 2D matrices of shape BatchSz*Chan*Heigh*Weidth
     the first two are copied for the output (ie. not depend on krnl, pads, etc)
     BatchSize is simply ignored, while Channels is iterated on
  *)
  let str = ref [] in
  let kshape,mp,ms,md = match n.IR.Vertex.operator_parameters with
    | Some (IR.Vertex.Pool_params ((Ksize k),s,p,d)) ->
      if List.length k > 2 then
        failwith "MaxPool kernels can only have two axes"
      else k,p,s,d
    | _ -> failwith "error, invalid arguments for maxpooling"
  in let pads  = match mp with
      | Some (IR.Vertex.Pads p) -> p
      | None -> failwith "TODO: implement where there are not strides"
  in let strides  = match ms with
      | Some (IR.Vertex.Stride s) -> s
      | None -> failwith "TODO: implement where there are not pads"
  in let dilations = match md with
      | Some (IR.Vertex.Dilations s) -> gen_list (List.length s) 1 []
      | None -> failwith "TODO: implement where there are not dilations"
  in
  let (a,adim) = match IR.preds g n with
    | [x_] -> let x = t_name x_ in
      let sh = get_shape_cfg x env in
      (x, sh)
    | _ -> failwith "MaxPool should have exactly 1 input" in
  let (bpads, epads) = half_split ((List.length pads)/2) [] pads in
  (* the following works for 2D shapes with BatchSz and Chan *)
  let (batchSz, chans, height, width) = match adim with
    | a::b::c::d::[] -> (a,b,c,d)
    | _ -> failwith "non 4axes shapes are not supported in MaxPool yet"
  in
  let tadim = [height; width]   (* truncated adim *) in
  let res_shape = (conv_output_shape_cfg tadim bpads epads
                     kshape strides dilations) in
  set_shape_cfg res (batchSz::chans::res_shape) env;
  let rec krnl_loop i kernel_shape chan sd2pidx acc to_maximize =
    (* this loop iterates on the kernel cells *)
    if kernel_shape = [] then
      let (strides, dilations, bpads, _ (* epads *)), res_idx = sd2pidx in
      let cell = gen_cell_cfg res_idx acc strides dilations bpads (* epads *) in
      if is_in_bound_cfg cell tadim then
        to_maximize := (a^"["^(stringify_int (0::chan::cell))^"],"):: !to_maximize
      else ()
    else
    if i < (List.hd kernel_shape) then
      (krnl_loop 0 (List.tl kernel_shape) chan sd2pidx (acc@[i]) to_maximize ;
       krnl_loop (i+1) kernel_shape chan sd2pidx acc to_maximize)
    else () in
  let rec res_loop i res_shape kernel_shape chan sd2p acc =
    (* This loops iterates on the result cells *)
    if res_shape = [] then
      (* ICI accumulateur, parce que pour chaque res_idx, une boucle sur tout
         le kernel avec des max *)
      (let to_maximize = ref [] in
       krnl_loop 0 kernel_shape chan (sd2p,acc) [] to_maximize;
       let ss = List.fold_right (fun i s -> i^s) !to_maximize "" in
       str := (res^(stringify_int (0::chan::acc))^"=max("^ss^")") :: !str
      )
    else
    if i < (List.hd res_shape) then
      (res_loop 0 (List.tl res_shape) kernel_shape chan sd2p (acc@[i]);
       res_loop (i+1) res_shape kernel_shape chan sd2p acc)
    else () in
  let rec chan_loop i c =
    (* this loops on channels *)
    if i >= c then
      ()
    else (
      res_loop 0 res_shape kshape i (strides, dilations,bpads, epads) [];
      chan_loop (i + 1) c
    ) in
  chan_loop 0 chans;
  !str


let pp_conv_cfg res n env g =
  (* Currently only applicable to 2D matrices of shape BatchSz*Chan*Heigh*Weidth
     the first two are copied for the output (ie. not depend on krnl, pads, etc)
     BatchSize is simply ignored, while Channels is iterated on
  *)
  let str = ref [] in
  let kshape,mp,ms,md = match n.IR.Vertex.operator_parameters with
    | Some (IR.Vertex.Pool_params ((Ksize k),s,p,d)) ->
      if List.length k > 2 then
        failwith "MaxPool kernels can only have two axes"
      else k,p,s,d
    | _ -> failwith "error, invalid arguments for maxpooling"
  in let pads  = match mp with
      | Some (IR.Vertex.Pads p) -> p
      | None -> failwith "TODO: implement where there are not strides"
  in let strides  = match ms with
      | Some (IR.Vertex.Stride s) -> s
      | None -> failwith "TODO: implement where there are not pads"
  in let dilations = match md with
      | Some (IR.Vertex.Dilations s) -> gen_list (List.length s) 1 []
      | None -> failwith "TODO: implement where there are not dilations"
  in
  let (a,adim, k, kdim, bias) = match IR.preds g n with
    | x::y::[] ->
      let ash = get_shape_cfg (t_name x) env in
      let bsh = get_shape_cfg (t_name y) env in
      ((t_name x), ash,(t_name y), bsh, None)
    | x::y::z::[] ->
      let ash = get_shape_cfg (t_name x) env in
      let bsh = get_shape_cfg (t_name y) env in
      let csh = get_shape_cfg (t_name z) env in
      if csh = [List.hd bsh] then
        ((t_name x), ash,(t_name y), bsh, Some (t_name z))
      else failwith "In Conv: size mismatch between ochan and bias dims"
    | _ -> failwith "Convolution should have exactly 2 or 3 inputs" in
  let (bpads, epads) = half_split ((List.length pads)/2) [] pads in
  (* the following works for 2D shapes with BatchSz and Chan *)
  let (batchSz, chans, height, width) = match adim with
    | e::b::c::d::[] -> (e,b,c,d)
    | _ -> failwith "non 4axes shapes are not supported in Conv yet"
  in
  let  (ochans, chansK, heightK, widthK) = match kdim with
    | a::b::c::d::[] -> (a,b,c,d)
    | _ -> failwith "non 4axes shapes are not supported in Conv yet"
  in
  let tadim = [height; width]   (* truncated adim *) in
  let tkdim = [heightK; widthK]   (* truncated wdim *) in
  if not (chans = chansK) then failwith "channels not equal in Conv"
  else ();
  let res_shape = conv_output_shape_cfg tadim bpads epads tkdim strides dilations in
  (* printf "res shape obtained. It's \n%!"; *)
  (* pp_shape res_shape; *)
  set_shape_cfg res (batchSz::ochans::res_shape) env;
  let rec ichan_loop i ic inCell kCell ochan to_sum =
    if i >= ic then
      ()
      (* printf "iteration ichan_loop unit over\n%!" *)
    else (
      (* printf "iteration ichan_loop over\n%!"; *)
      to_sum :=
        (a^"["^(stringify_int (0::i::inCell))^"]*"^
         k^"["^(stringify_int (ochan::i::kCell))^"],"):: !to_sum;
      ichan_loop (i + 1) ic inCell kCell ochan to_sum
    ) in
  let rec krnl_loop i kernel_shape ochan acc res_idx to_sum =
    (* printf "kernel_loop over\n%!"; *)
    (* this loop iterates on the kernel cells *)
    if kernel_shape = [] then
      let cell = gen_cell_cfg res_idx acc strides dilations bpads in
      if is_in_bound_cfg cell tadim then
        ichan_loop 0 chans cell acc ochan to_sum
      else ()
    else
    if i < (List.hd kernel_shape) then
      (krnl_loop 0 (List.tl kernel_shape) ochan (acc@[i]) res_idx to_sum ;
       krnl_loop (i+1) kernel_shape ochan acc res_idx to_sum)
    else () in
  let get_bias ochan_indx =
    match bias with
    | None -> ""
    | Some name -> name^"["^(string_of_int ochan_indx)^"] * " in
  let rec res_loop i res_sh ochan acc =
    (* This loops iterates on the result cells *)
    match res_sh with
    | [] ->
      (* at this point, acc contains the index of the res cell *)
      let to_sum = ref [] in
      krnl_loop 0 tkdim ochan [] acc to_sum;
      let ss = List.fold_right (fun i s -> i^s) !to_sum "" in
      str := (res^(stringify_int (0::ochan::acc))^
              "="^(get_bias ochan)^"SUM("^ss^")") :: !str
    | h::t ->
      if i <  h then begin
        res_loop 0 t ochan (acc@[i]);
        res_loop (i+1) res_sh ochan acc
      end
  in
  let rec ochans_loop i oc =
    (* this loops on channels *)
    (* printf "ochan over\n%!"; *)
    if i < oc then begin
      res_loop 0 res_shape i [];
      ochans_loop (i+1) oc
    end
  in
  ochans_loop 0 chans;
  (* printf "pping successful conv\n%!"; *)
  !str

(** Print the cnode using successor for name *)
let pp_cnode_cfg env (t:theory) g n =
  let succ_name = match IR.NierCFG.succ_e g n with
    | [(_,l,_)] -> l
    | _ -> failwith "Only one output for C_Nodes, for now"
  in
  match n.IR.Vertex.operator with
  | IR.Vertex.ReLu      -> pp_relu_cfg succ_name n env t g
  | IR.Vertex.Add       -> pp_add_cfg succ_name n env t g
  | IR.Vertex.Mul       -> pp_mul_cfg succ_name n env t g
  | IR.Vertex.Matmul    -> pp_matmul_cfg succ_name n env t g
  | IR.Vertex.Transpose -> pp_transpose_cfg succ_name n env g
  | IR.Vertex.MaxPool   -> pp_maxpool_cfg succ_name n env g
  | IR.Vertex.Conv      -> pp_conv_cfg succ_name n env g
  | IR.Vertex.Identity  -> pp_id_cfg succ_name n env g
  |_                    -> failwith "TODO operator in smtifyer.cnode"

(* Initialiser nodes are already in nier_graph.
   So in addition of inputs and ouptus, the NAMED vertices may come from them
   Therefore, I should add an option field in addition to size:
   if raw_data is present then add it, else none.
   Then, when going through cnode, if there is no raw, then treat like a variable
   (what I already did). Else, retrieve that data and use it.
   use Onnx_parser.get_tensor_raw_data with offset to get the float from
   the list. *)
let pp_IOnode_cfg (env:env_cfg) n =
  let name = match n.IR.Vertex.name with
    | Some n -> n
    | None -> "C_NODE"
  in
  env := (name, {shape = n.IR.Vertex.shape;data = n.IR.Vertex.tensor}) :: !env


let pp_graph_cfg g t =
  let env = env_init_cfg in
  let (gCNode_, gInOut_) =
    (* printf "List.partition\n%!"; *)
    List.partition (fun x -> match x.IR.Vertex.name with
        | Some _ -> false
        | None -> true) (IR.vertex_list g)
  in
  let gCNode = List.rev gCNode_ and gInOut = List.rev gInOut_ in
  (* printf "Length of gCNnode: %d \n%!" (List.length gCNode); *)
  (* printf "Length of gInOut: %d \n%!" (List.length gInOut); *)
  (* print_endline "gCNode:"; *)
  (* List.iter (fun x -> printf "%s, %d;" (t_name x) (x.IR.Vertex.id)) gCNode; *)
  (* print_endline "\ngInOut:"; *)
  (* List.iter (fun x -> printf "%s " (t_name x)) gInOut; *)
  List.iter (pp_IOnode_cfg env) gInOut;
  (* pp_env_cfg env; *)
  let cnodes = List.flatten (List.map (pp_cnode_cfg env t g) (gCNode)) in
  let variables_decl =
    try
      (";; Declaring variable")::(get_declarations_cfg env t)
    with Invalid_argument(_) -> printf "variables_decl failing\n";[] in
  printf "get_declarations successful\n%!";
  let functions_decl =
    try
      [";; Declaring functions"; relu_fun_decl t]
    with Invalid_argument(_) -> printf "functions_decl failing\n";[] in
  printf "functions_decl successful\n%!";
  let graph_decl =
    try
      ";; The graph"::cnodes
    with Invalid_argument(_) -> printf "graph_decl failing\n"; [] in
  let header =  pp_header t in
  (* pp_env_cfg env; *)
  printf "graph_decl successful\n%!";
  (* printf "\nLength of cnodes: %d \n%!" (List.length cnodes); *)
  (* printf "Length of variables_decl: %d \n%!" (List.length variables_decl); *)
  (* printf "Length of functions_decl: %d \n%!" (List.length functions_decl); *)
  (* printf "Length of graph_decl: %d \n%!" (List.length graph_decl); *)
  try
    (* Huge lists so List.rev_append instead of @ (which is not tail recursive *)
    latr (latr (latr (latr header variables_decl) functions_decl) graph_decl)  [";; Write property here"]
  with Invalid_argument(_) -> printf "writing of string failing\n"; []
