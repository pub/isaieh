(** This module provide tools to convert a valid NIER
    into a SMTLIB2 formula.

    NIER (defined in {!module:Nier_cfg}) encapsulate
    parameters in tensor
    forms and a computation graph with various operations.
    On the other hand, SMTLIB2 format only supports
    individual variables.

    This module provide tools to properly translate NIER
    data into SMTLIB2
    formula. Variables are stored inside of an environment,
    their shape being
    either provided by the NIER or inferred with the
    expected result
    of ONNX operations.

    @author Zakaria Chihani, Julien Girard
*)


module IR = Ir.Nier_cfg

(** {1 Definition of the supported theories
    for SMTLIB2 output format} *)

type theory = Real_Theory | Float_Theory | Linear_Real_Theory

(** {1 Environment utilities} *)
(** Environment stores shapes and raw data for all variables.
    An environment is a list of tuple (string; vdata) representing the name
    of the variable, its corresponding shape and the (potentially empty)
    raw data linked to that node *)

type vdata_cfg = {shape: IR.Vertex.shape ;data : IR.Tensor.t option}
type env_cfg = (string * vdata_cfg) list ref
val env_init_cfg : env_cfg
val pp_shape_cfg : int list -> unit
val pp_env_cfg : (string * vdata_cfg) list ref -> unit
val get_declarations_cfg : (string * vdata_cfg) list ref ->
  theory -> string list
(** [get_declarations_cfg env t ] build the string corresponding to the declaration
    of all variables in [env] and their value if necessary
    according to theory t *)

(** {1 Padding and broadcast utilities} *)
(** When computing operations with tensors of different shapes, it is sometimes
    necessary to perform padding and broadcasting operations.
    Padding add dimensions to an array on the left or on the right,
    if necessary.

    Broadcasting consists on modifying the dimensions of two arrays
    to obtain compatible dimensions without changing the underlying data.
    For instance, a matrix of shape [3,2] multiplied (matrix multiplication)
    with a vector of shape [2] is not a valid operation, but becomes valid
    if the shape of the vector is [2,1]. The operation will result on a
    vector of shape [2,1]. For more information,
    @see <https://github.com/onnx/onnx/blob/master/docs/Broadcasting.md> *)

val one_padding_cfg : int list -> int -> int list
val pad_left_cfg : int list -> int list
val pad_right_cfg : int list -> int list
val pad_cfg : int list -> int list -> int list * int list
val broadcast_cfg : int list -> int list -> int list
val getSerie : string -> string -> string list -> string
val get_fp_serie_node :
  string list -> string -> string list -> string -> string list

(** {1 ONNX Operators pretty printers and utilities } *)

val get_unary_op : string -> string -> string -> string list -> string list
val pp_relu_cfg : string -> IR.Vertex.t -> env_cfg
  -> theory -> IR.NierCFG.t -> string list

(** {2 Add } *)

val pp_add_cell_cfg :
  int list ->
  string ->
  int list ->
  string -> int list -> string -> int list -> theory ->
  string list ref -> unit
(** [pp_add_cell idx a adim b bdim c cdim res] build the string
    corresponding to the evaluation of the Add operation on
    matrix [a] of shape [adim] and [b] of shape [bdim], with
    matrix [c] of shape [cdim] storing the result *)

val pp_add_cfg : string -> IR.Vertex.t -> env_cfg ->
  theory -> IR.NierCFG.t -> string list

(** {2 Mul } *)

val pp_mul_cfg : string -> IR.Vertex.t -> env_cfg->
  theory -> IR.NierCFG.t ->  string list

(** {2 MatMul } *)
(** Various functions here to write correct flattened matrix multiplication  *)

val check_matmul_size_cfg : int list -> int list -> int * int list
val pp_matmul_cell :
  int list ->
  int -> int -> string -> int list -> string -> int list -> theory ->
  int -> string
val mat_mul_cell :
  int list ->
  int list ->
  string ->
  int list -> string -> int list -> string -> int -> string list ref ->
  theory -> unit
val pp_matmul_cfg :
  string -> IR.Vertex.t -> env_cfg->
  theory -> IR.NierCFG.t -> string list

(** {2 Transpose } *)

val pp_transpose_default : string -> string -> int list -> string list
val inversed_shape : int -> int list
val permute_idx_cfg : int array -> int array -> string
val perm_shape_cfg : int list -> int list -> int list
val pp_transpose_cfg :
  string -> IR.Vertex.t -> env_cfg -> IR.NierCFG.t
  -> string list

(** {1 Printer utilities } *)

val pp_cnode_cfg : env_cfg -> theory -> IR.NierCFG.t ->
  IR.Vertex.t -> string list

val pp_IOnode_cfg : env_cfg-> IR.Vertex.t -> unit

(** [pp_graph_cfg g t] transforms the computational
    graph under a string list with SMTLIB2 format,
    according to theory [t].
    Each element of the returned list is a line of SMTLIB2.*)

val pp_graph_cfg : IR.NierCFG.t -> theory -> string list
