(** This module provide tools to parse an ONNX file
    and build an intermediate representation (NIER).
    @author Zakaria Chihani, Julien Girard
*)

module NCFG = Ir.Nier_cfg
module Onnx_piqi = Piqi_interface.Onnx_piqi

(** [parse_model_from_file fp] returns the ONNX model
    stored in [fp] file,
    complying to the interface given by onnx_piqi.

    @param fp filepath, must point to an ONNX file
*)

val parse_model_from_file : string -> Onnx_piqi.Model_proto.t

(** [node_graph g] build a NIER-CFG from ONNX graph [g].

    This function get the required attributes from the
    ONNX graph and use them
    to build the corresponding NIER control flow graph.
    It performs a merge between the ONNX initializer
    (containing numerical
    parameters of the neural network) and the ONNX node
    graph (containing the computation graph
    of the neural network).
    Resulting NIER CFG thus contains both the structure
    and the
    parameters, as well as computation informations.

    @param g ONNX Node graph to compute into a NIER graph
*)

val produce_cfg : Onnx_piqi.Graph_proto.t -> NCFG.NierCFG.t

val nier_from_onnx : string -> NCFG.NierCFG.t

