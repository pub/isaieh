(* Note: In Onnx_piqi, 'external' has been replaced by 'externall' to avoid conflict with Ocaml
 * 'external' directive*)
module Onnx_piqi =  Piqi_interface.Onnx_piqi
open Onnx_piqi
module I64 = Int64
module NCFG = Ir.Nier_cfg
module N = NCFG.NierCFG
module MapStr = Map.Make(String)

exception ParseError of string
type tensordata = Raw of string | Float of float list
type op_attribute = Onnx_piqi.attribute_proto
let (no_attr : op_attribute) =
  {name = None; ref_attr_name = None; doc_string = None;
   type_ = None; f = None; i = None;
   s = None; t = None; g = None;
   floats = []; ints = []; strings = [];
   tensors = []; graphs = []  }
(* return true if l1 has at least one common element wih l2 *)
let rec shared_elm l1 l2 = match l1 with
  | x::y -> List.mem x l2 || shared_elm y l2
  | [] -> false

(* Utils to build NCFG nodes*)
let i64_to_shape l = List.map I64.to_int l
let unkerasize l =
  List.map (fun x -> if x = 0 then 1 else x) l

let unpack_tname x = match x.NCFG.Vertex.name with
  | Some n -> n
  | None -> ""

 (* Build operator parameters compliant to NIER CFG *)
let operator_parameters (attr:attribute_proto list) op = match op with
  | NCFG.Vertex.Transpose ->
    Some (NCFG.Vertex.Transpose_params (List.map (I64.to_int)
                                          (List.nth attr 0).ints))
  (*TODO: maxpool and conv operators: match attr.name in attributes to
   * create the correct value for each attribute*)
  (* | NCFG.Vertex.MaxPool -> *)
  (* | NCFG.Vertex.Conv -> *)
  | _ -> None
let rec build_op_param_list attrs ops l = match (attrs,ops) with
  | a::b,c::d -> build_op_param_list b d ((operator_parameters a c)::l)
  | [],[] -> List.rev l (*All other list constructions are folding right,
                        so we need to put a final revert *)
  | _ -> raise (ParseError "Error, operators and attributes list have not
  the same size")
let get_node_operator_cfg x = match x.Node_proto.op_type with
  | None -> NCFG.Vertex.NO_OP
  | Some o ->
    match o with
    | "Add" -> NCFG.Vertex.Add
    | "Mul" -> NCFG.Vertex.Mul
    | "Relu" -> NCFG.Vertex.ReLu
    | "MatMul" -> NCFG.Vertex.Matmul
    | "LogSoftmax" -> NCFG.Vertex.LogSoftmax
    | "Transpose" -> NCFG.Vertex.Transpose
    | "Squeeze" -> NCFG.Vertex.Squeeze
    | "MaxPool" -> NCFG.Vertex.MaxPool
    | "Conv" -> NCFG.Vertex.Conv
    | "Identity" -> NCFG.Vertex.Identity
    | _ -> failwith ("Unsupported ONNX Operator in Parser: "^o)
let fold_nodes_ops_cfg ns =
  List.fold_right (fun x l -> (get_node_operator_cfg x)::l) ns []
let fold_value_info_names_cfg vn =
  List.fold_right (fun x l -> match x.Value_info_proto.name with
      |Some  str -> (Some str)::l
      |None -> None::l) vn []

(*This function gets a 1-dimension list of data [raw], and returns the value that
 * would be stored at index [index] under a tensor of shape [sh] . [raw] must
 * be seen as the 1D flattened values of the tensor*)
let get_float_from_index index data sh =
  let pop_sh = (List.tl sh)@[1]
  in
  (* Returns the factors by which multiply each coordinate *)
  let rec get_factors_from_sh sh_f l = match sh_f with
    | [] -> List.rev l
    | _ -> get_factors_from_sh (List.tl sh_f) ((List.fold_left (fun x y -> x*y) 1 sh_f)::l)
  in
  let factors = get_factors_from_sh pop_sh []
  in
  let coord_in_data = List.fold_left2 (fun x y z -> x + y*z) 0 index factors
  in
  match data with
  | Raw raw -> let offset =
                 (* Each float is coded on 4 bytes*)
                 4*(coord_in_data)
    in
    (* Printf.printf "\nOffset %d\n" offset; *)
    let res = EndianBytes.LittleEndian.get_float (Bytes.of_string raw) offset
    in
    (*printf "\n Res %f\n" res; res *)
    res
  | Float f -> List.nth f (coord_in_data)
(* List.iter (Printf.printf "Index %d") index; *)
(* List.iter (Printf.printf "Size %d") sh; *)
(* List.iter (Printf.printf "Factors %d") factors; *)
(* Printf.printf "\nLength index %d" (List.length index); *)
(* Printf.printf "\nLength factor %d" (List.length factors); *)
(* Printf.printf "\nLength raw %d" (String.length raw); *)

(* a list of all possible coordinates for a given shape *)
let all_coords sh =
  let rec ranges acc shape = match shape with
    | x::y -> ranges ((List.init x (fun i -> i))::acc) y
    | [] -> acc
  (* a list containing a list of all possible indexes, for each dimension *)
  in let xxs = ranges [] sh in
  (* add to each element of the list of all possible coordinates all
   * possible indexes ... *)
  let aux acc xs = List.concat @@
    List.map (fun x -> List.map (fun lt -> x::lt) acc) xs
  (* ... for each dimension, starting from an empty list of
   * possible coordinates *)
  in List.fold_left aux [[]] xxs
let build_tensor_from_data sh_i64 data =
  let sh = List.map (Int64.to_int) sh_i64 in
  let tensor = NCFG.Tensor.create sh in
  let coords = all_coords tensor.shape in
  (* initiate tensor values as long as there is coordinates left to
   * initialize *)
  (* let shape_to_str = List.fold_left (fun x y -> x^","^(string_of_int y)) "" in *)
  (* let t_value_string t = *)
  (*   List.fold_left (fun acc l -> *)
  (*       acc^shape_to_str l^": "^ *)
  (*       (string_of_float (NCFG.Tensor.get_idx t l)^"\n")) *)
  (*     "POUET POUET POUET \n" coords *)
  (* in *)
  let rec init_tensor t idx r = match idx with
    | x::y ->
      let value = get_float_from_index x r t.NCFG.Tensor.shape in
      (* let value_t = NCFG.Tensor.get_idx t x in *)
      (* Printf.printf "Before affect: index: %s; value; %f, *)
      (* value in tensor: %f\n" *)
      (*   (shape_to_str x) value value_t; *)
        (* Printf.printf "Tensor before affect: %s" (t_value_string t); *)
      let t_new = NCFG.Tensor.set_idx t x value in
        (* Printf.printf "Tensor after affect: %s" (t_value_string t_new); *)
      (* Printf.printf "After affect: index: %s; value; %f\n" *)
        (* (shape_to_str x) (NCFG.Tensor.get_idx t_new x); *)
      init_tensor t_new y r
    | [] -> t
  in
  (* Printf.printf "%s"  (t_value_string (init_tensor tensor coords raw)); *)
  init_tensor tensor coords data

let dict_tensors_cfg ts =
  let t_name x = match x.Tensor_proto.name with
    | Some n -> n
    | None -> "C_NODE"
  in
  let t_dim x = x.Tensor_proto.dims in
  let t_data x = match x.Tensor_proto.raw_data with
    | Some rd -> Some (build_tensor_from_data (t_dim x) (Raw rd))
    | None -> begin match x.Tensor_proto.float_data with
        | [] -> None
        | f -> Some (build_tensor_from_data (t_dim x) (Float f))

      end
  in
  List.fold_left (fun m x -> MapStr.add (t_name x) (t_data x) m)
    MapStr.empty ts


(* Utils to generate empty values for the NIER CFG *)
let rec init_list f n i l = if i>=n then l else init_list f n (i+1) (f i :: l)
let rec single_value_list v n l =
  if n == 0 then l else single_value_list v (n-1) (v::l)
let no_op_cfg_list n = single_value_list NCFG.Vertex.NO_OP n  []
let no_datadim_list n = single_value_list [] n  []
let no_attr_list n = single_value_list [no_attr] n  []
let snd (_,a) = a
(* Utilities for Nier CFG *)
let get_node_attr x = x.Node_proto.attribute

(* Tensor processing utilities *)

let fold_value_info_names vn =
  List.fold_right (fun x l -> match x.Value_info_proto.name with
      |Some  str -> str::l
      |None -> ""::l) vn []
let value_info_dims vn =
  let val_t = (match vn.Value_info_proto.type_ with
      | Some t -> t
      | None -> failwith "No type in value info" )
  in
  let tns_t  = (match val_t.Type_proto.tensor_type with
      | Some t -> t
      | None -> failwith "No tensor type in value info")
  in
  let tns_s  = (match tns_t.Type_proto_tensor.shape with
      | Some s -> s
      | None -> failwith "No tensor shape in value info")
  in
  let dims = tns_s.Tensor_shape_proto.dim in
  List.fold_right
    (fun x l -> match x.Tensor_shape_proto_dimension.dim_value with
       | Some d -> (d)::l
       | None -> (I64.zero)::l) dims []
let fold_value_info_dims vn =
  List.fold_right (fun x l -> (value_info_dims x)::l) vn []
let fold_nodes_attr ns =
  List.fold_right (fun x l -> (get_node_attr x)::l) ns []
let fold_nodes_inputs ns =
  List.fold_right (fun x l -> (x.Node_proto.input)::l) ns []
let fold_nodes_outputs ns =
  List.fold_right (fun x l -> (x.Node_proto.output)::l) ns []
(*Dict containing initializer tensors dimensions and raw data*)
let parse_model_from_file fp =
  let ch = open_in_bin fp in
  let buf = Piqirun.init_from_channel ch in
  parse_model_proto buf

(* Build inputs, outputs and c_nodes attributes, put them in list,
 * then initiate data structure with all of this*)


let produce_cfg (g:Graph_proto.t) =
  let nodes = g.node
  and inputs = g.input
  and outputs = g.output
  and initi = g.initializer_
  in
  let i_nodes = fold_value_info_names_cfg inputs
  and o_nodes = fold_value_info_names_cfg outputs
  and c_nodes = single_value_list None (List.length nodes) []
  in
  let c_ops = fold_nodes_ops_cfg nodes
  and i_ops = no_op_cfg_list (List.length i_nodes)
  and o_ops = no_op_cfg_list (List.length o_nodes)
  in
  let c_attr = fold_nodes_attr nodes
  and i_attr = no_attr_list (List.length i_nodes)
  and o_attr = no_attr_list (List.length o_nodes)
  in
  let c_nodes_inputs = fold_nodes_inputs nodes
  and c_nodes_outputs = fold_nodes_outputs nodes
  and i_nodes_inputs =
    single_value_list ["NO_INPUT"] (List.length i_nodes) []
  and i_nodes_outputs =
    single_value_list ["NO_OUTPUT"] (List.length i_nodes) []
  and o_nodes_inputs =
    single_value_list ["NO_INPUT"] (List.length o_nodes) []
  and o_nodes_outputs =
    single_value_list ["NO_OUTPUT"] (List.length o_nodes) []
  in
  let data_dict = dict_tensors_cfg initi in
  (* With the current implementation of init_list, data_list must
   * be reversed to respect the order of the other lists*)
  let unpack v = match v with
    | Some v -> v
    | None -> failwith "error, unpack found an unexpected None"
  in
  let tensor_list = init_list
      (fun i -> try MapStr.find (unpack (List.nth i_nodes i)) data_dict
        with Not_found -> None)
      (List.length i_nodes) 0 [] in
  let tensor_list_full = MapStr.bindings data_dict in
  let tensor_list_rev = List.rev tensor_list in
  let c_tensordim_list = List.init (List.length nodes) (fun _ -> [])
  and c_tensorraw_list = List.init (List.length nodes) (fun _ -> None)
  and o_tensordim_list = fold_value_info_dims outputs
  and o_tensorraw_list = List.init (List.length o_nodes) (fun _ -> None)
  and i_tensordim_list = fold_value_info_dims inputs
  and i_tensorraw_list = tensor_list_rev
  in
  let nodes_names = i_nodes @ c_nodes @ o_nodes in
  let ops = i_ops
            @ c_ops
            @ o_ops  in
  let attrs = i_attr
              @ c_attr
              @ o_attr  in
  let prevs_list = i_nodes_inputs
                   @ c_nodes_inputs
                   @ o_nodes_inputs in
  let nexts_list = i_nodes_outputs
                   @ c_nodes_outputs
                   @ o_nodes_outputs in
  let tensor_dims = i_tensordim_list
                  @ c_tensordim_list
                  @ o_tensordim_list  in
  let tensors = i_tensorraw_list
                  @ c_tensorraw_list
                  @ o_tensorraw_list  in
  let op_params_cfg = build_op_param_list attrs ops [] in
  let cfg = NCFG.init_cfg in
  (* Printf.printf "%d\n" (List.length op_params_cfg); *)
  (* Printf.printf "%d\n" (List.length nodes_names); *)
  (* List.iter (fun x -> match x with *)
  (*     | Some s -> Printf.printf "%s\n" (NCFG.Vertex.str_op_params s) *)
  (*     | None -> Printf.printf "nothing\n" ) op_params_cfg; *)
  (* List.iter (fun x -> Printf.printf "%s\n" (NCFG.Vertex.str_op x) ) ops; *)
  (* adding inputs, outputs and cnodes to the cfg *)
  for i = 0 to ((List.length nodes_names) - 1) do
    let (v:NCFG.Vertex.t) = NCFG.Vertex.create
        ~id:i
        ~name:(List.nth nodes_names i)
        ~sh:(unkerasize (i64_to_shape (List.nth tensor_dims i)))
        ~op:(List.nth ops i)
        ~op_p:(List.nth op_params_cfg i)
        ~pred:(List.nth prevs_list i)
        ~succ:(List.nth nexts_list i)
        ~tensor:(List.nth tensors i)
    in
    N.add_vertex cfg v
  done;
  (** Adding edges between vertices (I am not proud of this code) **)

  let rec exclude l1 l2 = match l1 with
    | x::y -> if List.mem x l2 then exclude y l2 else (x::exclude y l2)
    | [] -> []
  in
  (* For each unnamed vertex (= a calculation node) in the cfg ... *)
  List.iter (fun (v:NCFG.Vertex.t) -> match v.name with
      | None ->
        let pred = v.pred
        and succ = v.succ
        in
        let prev_v =
          (* ... get all vertices in cfg that have the current vertex preds
           * in their succ (at least one of their succ is inside our preds )*)
          NCFG.find_vertices cfg
            (fun (x:NCFG.Vertex.t) ->
                  if shared_elm pred x.succ then true else false)
          (* ... get all vertices in cfg that have the current vertex preds
           * in their name (they are named the same as one of our preds )*)
        and named_pred =
          NCFG.find_vertices cfg
            (fun (x:NCFG.Vertex.t) -> match x.name with
               | Some name -> if shared_elm pred [name]
                 then true else false
               | None -> false)
          (* ... get all vertices in cfg that have the current vertex succ
           * in their name (they are named the same as one of our succs )*)
        and named_succ =
          NCFG.find_vertices cfg
            (fun (x:NCFG.Vertex.t) -> match x.name with
               | Some name -> if shared_elm succ [name]
                 then true else false
               | None -> false)
          (* get all vertices in cfg that have the current vertex succs
           * in their preds (at least one of their preds is inside our succ )*)
        and next_v =
          NCFG.find_vertices cfg
            (fun (x:NCFG.Vertex.t) ->
               if shared_elm succ x.pred then true else false)
        in
        (* add edges between current vertex and identified preds and succs *)
        let v_predecessors = prev_v@named_pred
        and v_successors = next_v@named_succ
        in
        List.iter (fun (x:NCFG.Vertex.t) ->
            let label = match List.nth x.succ 0 with
              | "NO_OUTPUT" ->
                let pred_name = unpack_tname x in
                if List.mem pred_name v.NCFG.Vertex.pred
                then pred_name
                else ""
              | l -> l
            in NCFG.NierCFG.add_edge_e
              cfg (x,label, v)) (v_predecessors);
        (* add successors edges after filtering those
         * that are already an edge*)
        List.iter (fun (x:NCFG.Vertex.t) ->
            let all_preds = NCFG.preds cfg x
            and all_succs = NCFG.succs cfg x in
            if List.mem v all_preds || List.mem v all_succs
            then ()
            else
              let label = match List.nth x.pred 0 with
                | "NO_INPUT" ->
                  let succ_name = unpack_tname x in
                  if List.mem succ_name v.NCFG.Vertex.succ
                  then succ_name
                  else ""
                | l -> l
              in
              NCFG.NierCFG.add_edge_e cfg (v,label,x)) (v_successors);
      | _ -> () ) (NCFG.vertex_list cfg);
  (*rationale of the following:
   * PyTorch stores network nodes in the field "inputs" of
   * the ONNX graph g, and network parameters as a list of tensors
   * in the ONNX initializer_.
   * To make the two correspond, elements of g.inputs and g.initializer_
   * share the same value in the field "name".
   * In Keras, elements of g.initializer_ have a name, but they do not
   * correspond to any name in g.inputs.
   * What we did before was then to create the actual nier cfg following the
   * PyTorch way.
   * Below, we complete the cfg with keras data by doing the following:
   *  * create a node for NIER for each tensor in onnx initializer_
   *  * for each NIER node, check if there is a node sharing the same name
   *    pred
   *  * if yes, remove the one with highest ID (those are initi nodes, but since
   *  there is already a node in CFG with this name we do not
   *  need those)
   *  * if not, for each NIER node, chck if there is a node
   *  which name is contained in prevs. add it to the prev
   * that way, the initializer_ tensors will be inside NIER and will be used
   * correctly by smtifyer.
   * *)


  (* adding initi vertices to the cfg *)
  for i = 0 to ((List.length tensor_list_full) - 1) do
    let shape =
      match (snd (List.nth tensor_list_full i)) with
      | Some t -> unkerasize t.shape
      | None -> []
    in
    let (v:NCFG.Vertex.t) = NCFG.Vertex.create
        ~id:(i+(List.length nodes_names))
        ~name:(Some (fst (List.nth tensor_list_full i)))
        ~sh:(unkerasize shape)
        ~op:NO_OP
        ~op_p:None
        ~pred:[]
        ~succ:[]
        ~tensor:(snd (List.nth tensor_list_full i)) in
    N.add_vertex cfg v
  done;
  (* build a list of nodes
   * sharing name but with different ids *)
  let same_name_diff_ids =
    let aux (x:NCFG.Vertex.t) = N.fold_vertex
        (fun y acc ->
           if y.name = x.name
           && not (y.id = x.id)
           && not (x.name = None) && not (x.name = None)
           then (x,y)::acc
           else acc) cfg []
    in
    N.fold_vertex (fun x l -> (aux x)::l) cfg []
  in
  let highest_ids =
    List.fold_left (fun acc x -> match x with
        | l::_ -> let maxval = max ((fst l).NCFG.Vertex.id)
                       ((snd l).NCFG.Vertex.id)
          in maxval::acc
        | [] -> acc) [] same_name_diff_ids
  in
  (* (* removing nodes with highest id, those are the*)
  (*  * ones we just added *)*)
  List.iter (fun x -> match x with
      | l::_ -> let v1 = fst l and v2 = snd l in
        if List.mem v1.NCFG.Vertex.id highest_ids then
          begin
            (* Printf.printf "Removing id %d \n%!" *)
            (*   v1.NCFG.Vertex.id; *)
            N.remove_vertex cfg v1
          end
        else ()
      | [] -> ())
    same_name_diff_ids;
  (* Now it is Keras time.
   * Look for nodes sharing name and preds,
   * then create edge *)
    let shared_name_preds =
      let aux (x:NCFG.Vertex.t) = match x.name with
        (* look in other vertices if name is among
         * predecessors *)
        | Some n -> NCFG.find_vertices cfg
                      (fun x -> shared_elm [n] x.pred)
        | None -> []
      in
      N.fold_vertex (fun x l -> (x,aux x)::l) cfg []
    in
    List.iter (fun x ->
        let orgn = fst x and to_edge = snd x in
        (* Printf.printf "Adding edge\n%!"; *)
        (* Printf.printf "orgn: %s" orgn.NCFG.Vertex.name; *)
        (* List.iter (fun p -> Printf.printf " %s " p.NCFG.Vertex.pred) *)
        (*   to_edge; *)
        List.iter (fun t ->
            if not (N.mem_edge cfg orgn t)
            then N.add_edge_e cfg
                (orgn,(unpack orgn.NCFG.Vertex.name),t)
            else ())
          to_edge)
      shared_name_preds;
  (* else (); *)
    cfg

let nier_from_onnx input_file =
  let model = parse_model_from_file input_file in
  let graph = match model.Model_proto.graph with
    | Some g -> g
    | None -> raise (ParseError
                       "No graph in ONNX input file!")
  in produce_cfg graph
