module rec Onnx_piqi :
  sig
    type float32 = float
    type float64 = float
    type uint64 = int64
    type protobuf_int64 = int64
    type binary = string
    type protobuf_int32 = int32
    type attribute_proto_attribute_type =
        [ `float
        | `floats
        | `graph
        | `graphs
        | `int
        | `ints
        | `string
        | `strings
        | `tensor
        | `tensors
        | `undefined ]
    type tensor_proto_data_type =
        [ `bfloat16
        | `bool
        | `complex128
        | `complex64
        | `double
        | `float
        | `float16
        | `int16
        | `int32
        | `int64
        | `int8
        | `string
        | `uint16
        | `uint32
        | `uint64
        | `uint8
        | `undefined ]
    type tensor_proto_data_location = [ `default | `externall ]
    type version =
        [ `ir_version
        | `ir_version_2017_10_10
        | `ir_version_2017_10_30
        | `ir_version_2017_11_3
        | `start_version ]
    type attribute_proto = Attribute_proto.t
    type value_info_proto = Value_info_proto.t
    type node_proto = Node_proto.t
    type model_proto = Model_proto.t
    type string_string_entry_proto = String_string_entry_proto.t
    type graph_proto = Graph_proto.t
    type tensor_proto = Tensor_proto.t
    type tensor_proto_segment = Tensor_proto_segment.t
    type tensor_shape_proto = Tensor_shape_proto.t
    type tensor_shape_proto_dimension = Tensor_shape_proto_dimension.t
    type type_proto = Type_proto.t
    type type_proto_tensor = Type_proto_tensor.t
    type operator_set_id_proto = Operator_set_id_proto.t
  end
and Attribute_proto :
  sig
    type t = {
      mutable name : string option;
      mutable ref_attr_name : string option;
      mutable doc_string : string option;
      mutable type_ : Onnx_piqi.attribute_proto_attribute_type option;
      mutable f : Onnx_piqi.float32 option;
      mutable i : Onnx_piqi.protobuf_int64 option;
      mutable s : Onnx_piqi.binary option;
      mutable t : Onnx_piqi.tensor_proto option;
      mutable g : Onnx_piqi.graph_proto option;
      mutable floats : Onnx_piqi.float32 list;
      mutable ints : Onnx_piqi.protobuf_int64 list;
      mutable strings : Onnx_piqi.binary list;
      mutable tensors : Onnx_piqi.tensor_proto list;
      mutable graphs : Onnx_piqi.graph_proto list;
    }
  end
and Value_info_proto :
  sig
    type t = {
      mutable name : string option;
      mutable type_ : Onnx_piqi.type_proto option;
      mutable doc_string : string option;
    }
  end
and Node_proto :
  sig
    type t = {
      mutable input : string list;
      mutable output : string list;
      mutable name : string option;
      mutable op_type : string option;
      mutable domain : string option;
      mutable attribute : Onnx_piqi.attribute_proto list;
      mutable doc_string : string option;
    }
  end
and Model_proto :
  sig
    type t = {
      mutable ir_version : Onnx_piqi.protobuf_int64 option;
      mutable opset_import : Onnx_piqi.operator_set_id_proto list;
      mutable producer_name : string option;
      mutable producer_version : string option;
      mutable domain : string option;
      mutable model_version : Onnx_piqi.protobuf_int64 option;
      mutable doc_string : string option;
      mutable graph : Onnx_piqi.graph_proto option;
      mutable metadata_props : Onnx_piqi.string_string_entry_proto list;
    }
  end
and String_string_entry_proto :
  sig
    type t = { mutable key : string option; mutable value : string option; }
  end
and Graph_proto :
  sig
    type t = {
      mutable node : Onnx_piqi.node_proto list;
      mutable name : string option;
      mutable initializer_ : Onnx_piqi.tensor_proto list;
      mutable doc_string : string option;
      mutable input : Onnx_piqi.value_info_proto list;
      mutable output : Onnx_piqi.value_info_proto list;
      mutable value_info : Onnx_piqi.value_info_proto list;
    }
  end
and Tensor_proto :
  sig
    type t = {
      mutable dims : Onnx_piqi.protobuf_int64 list;
      mutable data_type : Onnx_piqi.protobuf_int32 option;
      mutable segment : Onnx_piqi.tensor_proto_segment option;
      mutable float_data : Onnx_piqi.float32 list;
      mutable int32_data : Onnx_piqi.protobuf_int32 list;
      mutable string_data : Onnx_piqi.binary list;
      mutable int64_data : Onnx_piqi.protobuf_int64 list;
      mutable name : string option;
      mutable doc_string : string option;
      mutable raw_data : Onnx_piqi.binary option;
      mutable external_data : Onnx_piqi.string_string_entry_proto list;
      mutable data_location : Onnx_piqi.tensor_proto_data_location option;
      mutable double_data : Onnx_piqi.float64 list;
      mutable uint64_data : Onnx_piqi.uint64 list;
    }
  end
and Tensor_proto_segment :
  sig
    type t = {
      mutable begin_ : Onnx_piqi.protobuf_int64 option;
      mutable end_ : Onnx_piqi.protobuf_int64 option;
    }
  end
and Tensor_shape_proto :
  sig
    type t = { mutable dim : Onnx_piqi.tensor_shape_proto_dimension list; }
  end
and Tensor_shape_proto_dimension :
  sig
    type t = {
      mutable dim_value : Onnx_piqi.protobuf_int64 option;
      mutable dim_param : string option;
      mutable denotation : string option;
    }
  end
and Type_proto :
  sig
    type t = {
      mutable tensor_type : Onnx_piqi.type_proto_tensor option;
      mutable denotation : string option;
    }
  end
and Type_proto_tensor :
  sig
    type t = {
      mutable elem_type : Onnx_piqi.protobuf_int32 option;
      mutable shape : Onnx_piqi.tensor_shape_proto option;
    }
  end
and Operator_set_id_proto :
  sig
    type t = {
      mutable domain : string option;
      mutable version : Onnx_piqi.protobuf_int64 option;
    }
  end
val parse_int64 : Piqirun.t -> int64
val packed_parse_int64 : Piqirun.IBuf.t -> int64
val parse_int32 : Piqirun.t -> int32
val packed_parse_int32 : Piqirun.IBuf.t -> int32
val parse_string : Piqirun.t -> string
val parse_float32 : Piqirun.t -> Onnx_piqi.float32
val packed_parse_float32 : Piqirun.IBuf.t -> Onnx_piqi.float32
val parse_protobuf_int64 : Piqirun.t -> Onnx_piqi.protobuf_int64
val packed_parse_protobuf_int64 : Piqirun.IBuf.t -> Onnx_piqi.protobuf_int64
val parse_binary : Piqirun.t -> Onnx_piqi.binary
val parse_protobuf_int32 : Piqirun.t -> Onnx_piqi.protobuf_int32
val packed_parse_protobuf_int32 : Piqirun.IBuf.t -> Onnx_piqi.protobuf_int32
val parse_float64 : Piqirun.t -> Onnx_piqi.float64
val packed_parse_float64 : Piqirun.IBuf.t -> Onnx_piqi.float64
val parse_uint64 : Piqirun.t -> Onnx_piqi.uint64
val packed_parse_uint64 : Piqirun.IBuf.t -> Onnx_piqi.uint64
val parse_attribute_proto : Piqirun.t -> Onnx_piqi.attribute_proto
val parse_attribute_proto_attribute_type :
  Piqirun.t -> Onnx_piqi.attribute_proto_attribute_type
val packed_parse_attribute_proto_attribute_type :
  Piqirun.IBuf.t ->
  [> `float
   | `floats
   | `graph
   | `graphs
   | `int
   | `ints
   | `string
   | `strings
   | `tensor
   | `tensors
   | `undefined ]
val parse_value_info_proto : Piqirun.t -> Onnx_piqi.value_info_proto
val parse_node_proto : Piqirun.t -> Onnx_piqi.node_proto
val parse_model_proto : Piqirun.t -> Model_proto.t
val parse_string_string_entry_proto :
  Piqirun.t -> Onnx_piqi.string_string_entry_proto
val parse_graph_proto : Piqirun.t -> Onnx_piqi.graph_proto
val parse_tensor_proto : Piqirun.t -> Onnx_piqi.tensor_proto
val parse_tensor_proto_segment : Piqirun.t -> Onnx_piqi.tensor_proto_segment
val parse_tensor_proto_data_type :
  Piqirun.t ->
  [> `bfloat16
   | `bool
   | `complex128
   | `complex64
   | `double
   | `float
   | `float16
   | `int16
   | `int32
   | `int64
   | `int8
   | `string
   | `uint16
   | `uint32
   | `uint64
   | `uint8
   | `undefined ]
val packed_parse_tensor_proto_data_type :
  Piqirun.IBuf.t ->
  [> `bfloat16
   | `bool
   | `complex128
   | `complex64
   | `double
   | `float
   | `float16
   | `int16
   | `int32
   | `int64
   | `int8
   | `string
   | `uint16
   | `uint32
   | `uint64
   | `uint8
   | `undefined ]
val parse_tensor_proto_data_location :
  Piqirun.t -> Onnx_piqi.tensor_proto_data_location
val packed_parse_tensor_proto_data_location :
  Piqirun.IBuf.t -> [> `default | `externall ]
val parse_tensor_shape_proto : Piqirun.t -> Onnx_piqi.tensor_shape_proto
val parse_tensor_shape_proto_dimension :
  Piqirun.t -> Onnx_piqi.tensor_shape_proto_dimension
val parse_type_proto : Piqirun.t -> Onnx_piqi.type_proto
val parse_type_proto_tensor : Piqirun.t -> Onnx_piqi.type_proto_tensor
val parse_operator_set_id_proto :
  Piqirun.t -> Onnx_piqi.operator_set_id_proto
val parse_version :
  Piqirun.t ->
  [> `ir_version
   | `ir_version_2017_10_10
   | `ir_version_2017_10_30
   | `ir_version_2017_11_3
   | `start_version ]
val packed_parse_version :
  Piqirun.IBuf.t ->
  [> `ir_version
   | `ir_version_2017_10_10
   | `ir_version_2017_10_30
   | `ir_version_2017_11_3
   | `start_version ]
val gen__int64 : int -> int64 -> Piqirun.OBuf.t
val packed_gen__int64 : int64 -> Piqirun.OBuf.t
val gen__int32 : int -> int32 -> Piqirun.OBuf.t
val packed_gen__int32 : int32 -> Piqirun.OBuf.t
val gen__string : int -> string -> Piqirun.OBuf.t
val gen__float32 : int -> Onnx_piqi.float32 -> Piqirun.OBuf.t
val packed_gen__float32 : Onnx_piqi.float32 -> Piqirun.OBuf.t
val gen__protobuf_int64 : int -> Onnx_piqi.protobuf_int64 -> Piqirun.OBuf.t
val packed_gen__protobuf_int64 : Onnx_piqi.protobuf_int64 -> Piqirun.OBuf.t
val gen__binary : int -> Onnx_piqi.binary -> Piqirun.OBuf.t
val gen__protobuf_int32 : int -> Int32.t -> Piqirun.OBuf.t
val packed_gen__protobuf_int32 : Int32.t -> Piqirun.OBuf.t
val gen__float64 : int -> float -> Piqirun.OBuf.t
val packed_gen__float64 : Onnx_piqi.float64 -> Piqirun.OBuf.t
val gen__uint64 : int -> int64 -> Piqirun.OBuf.t
val packed_gen__uint64 : Onnx_piqi.uint64 -> Piqirun.OBuf.t
val gen__attribute_proto : int -> Onnx_piqi.attribute_proto -> Piqirun.OBuf.t
val gen__attribute_proto_attribute_type :
  int -> Onnx_piqi.attribute_proto_attribute_type -> Piqirun.OBuf.t
val packed_gen__attribute_proto_attribute_type :
  [< `float
   | `floats
   | `graph
   | `graphs
   | `int
   | `ints
   | `string
   | `strings
   | `tensor
   | `tensors
   | `undefined ] ->
  Piqirun.OBuf.t
val gen__value_info_proto :
  int -> Onnx_piqi.value_info_proto -> Piqirun.OBuf.t
val gen__node_proto : int -> Onnx_piqi.node_proto -> Piqirun.OBuf.t
val gen__model_proto : int -> Model_proto.t -> Piqirun.OBuf.t
val gen__string_string_entry_proto :
  int -> Onnx_piqi.string_string_entry_proto -> Piqirun.OBuf.t
val gen__graph_proto : int -> Onnx_piqi.graph_proto -> Piqirun.OBuf.t
val gen__tensor_proto : int -> Onnx_piqi.tensor_proto -> Piqirun.OBuf.t
val gen__tensor_proto_segment :
  int -> Onnx_piqi.tensor_proto_segment -> Piqirun.OBuf.t
val gen__tensor_proto_data_type :
  int ->
  [< `bfloat16
   | `bool
   | `complex128
   | `complex64
   | `double
   | `float
   | `float16
   | `int16
   | `int32
   | `int64
   | `int8
   | `string
   | `uint16
   | `uint32
   | `uint64
   | `uint8
   | `undefined ] ->
  Piqirun.OBuf.t
val packed_gen__tensor_proto_data_type :
  [< `bfloat16
   | `bool
   | `complex128
   | `complex64
   | `double
   | `float
   | `float16
   | `int16
   | `int32
   | `int64
   | `int8
   | `string
   | `uint16
   | `uint32
   | `uint64
   | `uint8
   | `undefined ] ->
  Piqirun.OBuf.t
val gen__tensor_proto_data_location :
  int -> Onnx_piqi.tensor_proto_data_location -> Piqirun.OBuf.t
val packed_gen__tensor_proto_data_location :
  [< `default | `externall ] -> Piqirun.OBuf.t
val gen__tensor_shape_proto :
  int -> Onnx_piqi.tensor_shape_proto -> Piqirun.OBuf.t
val gen__tensor_shape_proto_dimension :
  int -> Onnx_piqi.tensor_shape_proto_dimension -> Piqirun.OBuf.t
val gen__type_proto : int -> Onnx_piqi.type_proto -> Piqirun.OBuf.t
val gen__type_proto_tensor :
  int -> Onnx_piqi.type_proto_tensor -> Piqirun.OBuf.t
val gen__operator_set_id_proto :
  int -> Onnx_piqi.operator_set_id_proto -> Piqirun.OBuf.t
val gen__version :
  int ->
  [< `ir_version
   | `ir_version_2017_10_10
   | `ir_version_2017_10_30
   | `ir_version_2017_11_3
   | `start_version ] ->
  Piqirun.OBuf.t
val packed_gen__version :
  [< `ir_version
   | `ir_version_2017_10_10
   | `ir_version_2017_10_30
   | `ir_version_2017_11_3
   | `start_version ] ->
  Piqirun.OBuf.t
val gen_int64 : int64 -> Piqirun.OBuf.t
val gen_int32 : int32 -> Piqirun.OBuf.t
val gen_string : string -> Piqirun.OBuf.t
val gen_float32 : Onnx_piqi.float32 -> Piqirun.OBuf.t
val gen_protobuf_int64 : Onnx_piqi.protobuf_int64 -> Piqirun.OBuf.t
val gen_binary : Onnx_piqi.binary -> Piqirun.OBuf.t
val gen_protobuf_int32 : Int32.t -> Piqirun.OBuf.t
val gen_float64 : float -> Piqirun.OBuf.t
val gen_uint64 : int64 -> Piqirun.OBuf.t
val gen_attribute_proto : Onnx_piqi.attribute_proto -> Piqirun.OBuf.t
val gen_attribute_proto_attribute_type :
  Onnx_piqi.attribute_proto_attribute_type -> Piqirun.OBuf.t
val gen_value_info_proto : Onnx_piqi.value_info_proto -> Piqirun.OBuf.t
val gen_node_proto : Onnx_piqi.node_proto -> Piqirun.OBuf.t
val gen_model_proto : Model_proto.t -> Piqirun.OBuf.t
val gen_string_string_entry_proto :
  Onnx_piqi.string_string_entry_proto -> Piqirun.OBuf.t
val gen_graph_proto : Onnx_piqi.graph_proto -> Piqirun.OBuf.t
val gen_tensor_proto : Onnx_piqi.tensor_proto -> Piqirun.OBuf.t
val gen_tensor_proto_segment :
  Onnx_piqi.tensor_proto_segment -> Piqirun.OBuf.t
val gen_tensor_proto_data_type :
  [< `bfloat16
   | `bool
   | `complex128
   | `complex64
   | `double
   | `float
   | `float16
   | `int16
   | `int32
   | `int64
   | `int8
   | `string
   | `uint16
   | `uint32
   | `uint64
   | `uint8
   | `undefined ] ->
  Piqirun.OBuf.t
val gen_tensor_proto_data_location :
  Onnx_piqi.tensor_proto_data_location -> Piqirun.OBuf.t
val gen_tensor_shape_proto : Onnx_piqi.tensor_shape_proto -> Piqirun.OBuf.t
val gen_tensor_shape_proto_dimension :
  Onnx_piqi.tensor_shape_proto_dimension -> Piqirun.OBuf.t
val gen_type_proto : Onnx_piqi.type_proto -> Piqirun.OBuf.t
val gen_type_proto_tensor : Onnx_piqi.type_proto_tensor -> Piqirun.OBuf.t
val gen_operator_set_id_proto :
  Onnx_piqi.operator_set_id_proto -> Piqirun.OBuf.t
val gen_version :
  [< `ir_version
   | `ir_version_2017_10_10
   | `ir_version_2017_10_30
   | `ir_version_2017_11_3
   | `start_version ] ->
  Piqirun.OBuf.t
val default_int64 : unit -> int64
val default_int32 : unit -> int32
val default_string : unit -> string
val default_float32 : unit -> float
val default_protobuf_int64 : unit -> int64
val default_binary : unit -> string
val default_protobuf_int32 : unit -> int32
val default_float64 : unit -> float
val default_uint64 : unit -> int64
val default_attribute_proto : unit -> Attribute_proto.t
val default_attribute_proto_attribute_type : unit -> [> `undefined ]
val default_value_info_proto : unit -> Value_info_proto.t
val default_node_proto : unit -> Node_proto.t
val default_model_proto : unit -> Model_proto.t
val default_string_string_entry_proto : unit -> String_string_entry_proto.t
val default_graph_proto : unit -> Graph_proto.t
val default_tensor_proto : unit -> Tensor_proto.t
val default_tensor_proto_segment : unit -> Tensor_proto_segment.t
val default_tensor_proto_data_type : unit -> [> `undefined ]
val default_tensor_proto_data_location : unit -> [> `default ]
val default_tensor_shape_proto : unit -> Tensor_shape_proto.t
val default_tensor_shape_proto_dimension :
  unit -> Tensor_shape_proto_dimension.t
val default_type_proto : unit -> Type_proto.t
val default_type_proto_tensor : unit -> Type_proto_tensor.t
val default_operator_set_id_proto : unit -> Operator_set_id_proto.t
val default_version : unit -> [> `start_version ]
type float32 = float
type float64 = float
type uint64 = int64
type protobuf_int64 = int64
type binary = string
type protobuf_int32 = int32
type attribute_proto_attribute_type =
    [ `float
    | `floats
    | `graph
    | `graphs
    | `int
    | `ints
    | `string
    | `strings
    | `tensor
    | `tensors
    | `undefined ]
type tensor_proto_data_type =
    [ `bfloat16
    | `bool
    | `complex128
    | `complex64
    | `double
    | `float
    | `float16
    | `int16
    | `int32
    | `int64
    | `int8
    | `string
    | `uint16
    | `uint32
    | `uint64
    | `uint8
    | `undefined ]
type tensor_proto_data_location = [ `default | `externall ]
type version =
    [ `ir_version
    | `ir_version_2017_10_10
    | `ir_version_2017_10_30
    | `ir_version_2017_11_3
    | `start_version ]
type attribute_proto = Attribute_proto.t
type value_info_proto = Value_info_proto.t
type node_proto = Node_proto.t
type model_proto = Model_proto.t
type string_string_entry_proto = String_string_entry_proto.t
type graph_proto = Graph_proto.t
type tensor_proto = Tensor_proto.t
type tensor_proto_segment = Tensor_proto_segment.t
type tensor_shape_proto = Tensor_shape_proto.t
type tensor_shape_proto_dimension = Tensor_shape_proto_dimension.t
type type_proto = Type_proto.t
type type_proto_tensor = Type_proto_tensor.t
type operator_set_id_proto = Operator_set_id_proto.t
