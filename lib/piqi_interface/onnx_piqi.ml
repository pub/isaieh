module rec Onnx_piqi:
  sig
    type float32 = float
    type float64 = float
    type uint64 = int64
    type protobuf_int64 = int64
    type binary = string
    type protobuf_int32 = int32
    type attribute_proto_attribute_type =
      [
        | `undefined
        | `float
        | `int
        | `string
        | `tensor
        | `graph
        | `floats
        | `ints
        | `strings
        | `tensors
        | `graphs
      ]
    type tensor_proto_data_type =
      [
        | `undefined
        | `float
        | `uint8
        | `int8
        | `uint16
        | `int16
        | `int32
        | `int64
        | `string
        | `bool
        | `float16
        | `double
        | `uint32
        | `uint64
        | `complex64
        | `complex128
        | `bfloat16
      ]
    type tensor_proto_data_location =
      [
        | `default
        | `externall
      ]
    type version =
      [
        | `start_version
        | `ir_version_2017_10_10
        | `ir_version_2017_10_30
        | `ir_version_2017_11_3
        | `ir_version
      ]
    type attribute_proto = Attribute_proto.t
    type value_info_proto = Value_info_proto.t
    type node_proto = Node_proto.t
    type model_proto = Model_proto.t
    type string_string_entry_proto = String_string_entry_proto.t
    type graph_proto = Graph_proto.t
    type tensor_proto = Tensor_proto.t
    type tensor_proto_segment = Tensor_proto_segment.t
    type tensor_shape_proto = Tensor_shape_proto.t
    type tensor_shape_proto_dimension = Tensor_shape_proto_dimension.t
    type type_proto = Type_proto.t
    type type_proto_tensor = Type_proto_tensor.t
    type operator_set_id_proto = Operator_set_id_proto.t
  end = Onnx_piqi
and Attribute_proto:
  sig
    type t = {
      mutable name: string option;
      mutable ref_attr_name: string option;
      mutable doc_string: string option;
      mutable type_: Onnx_piqi.attribute_proto_attribute_type option;
      mutable f: Onnx_piqi.float32 option;
      mutable i: Onnx_piqi.protobuf_int64 option;
      mutable s: Onnx_piqi.binary option;
      mutable t: Onnx_piqi.tensor_proto option;
      mutable g: Onnx_piqi.graph_proto option;
      mutable floats: Onnx_piqi.float32 list;
      mutable ints: Onnx_piqi.protobuf_int64 list;
      mutable strings: Onnx_piqi.binary list;
      mutable tensors: Onnx_piqi.tensor_proto list;
      mutable graphs: Onnx_piqi.graph_proto list;
    }
  end = Attribute_proto
and Value_info_proto:
  sig
    type t = {
      mutable name: string option;
      mutable type_: Onnx_piqi.type_proto option;
      mutable doc_string: string option;
    }
  end = Value_info_proto
and Node_proto:
  sig
    type t = {
      mutable input: string list;
      mutable output: string list;
      mutable name: string option;
      mutable op_type: string option;
      mutable domain: string option;
      mutable attribute: Onnx_piqi.attribute_proto list;
      mutable doc_string: string option;
    }
  end = Node_proto
and Model_proto:
  sig
    type t = {
      mutable ir_version: Onnx_piqi.protobuf_int64 option;
      mutable opset_import: Onnx_piqi.operator_set_id_proto list;
      mutable producer_name: string option;
      mutable producer_version: string option;
      mutable domain: string option;
      mutable model_version: Onnx_piqi.protobuf_int64 option;
      mutable doc_string: string option;
      mutable graph: Onnx_piqi.graph_proto option;
      mutable metadata_props: Onnx_piqi.string_string_entry_proto list;
    }
  end = Model_proto
and String_string_entry_proto:
  sig
    type t = {
      mutable key: string option;
      mutable value: string option;
    }
  end = String_string_entry_proto
and Graph_proto:
  sig
    type t = {
      mutable node: Onnx_piqi.node_proto list;
      mutable name: string option;
      mutable initializer_: Onnx_piqi.tensor_proto list;
      mutable doc_string: string option;
      mutable input: Onnx_piqi.value_info_proto list;
      mutable output: Onnx_piqi.value_info_proto list;
      mutable value_info: Onnx_piqi.value_info_proto list;
    }
  end = Graph_proto
and Tensor_proto:
  sig
    type t = {
      mutable dims: Onnx_piqi.protobuf_int64 list;
      mutable data_type: Onnx_piqi.protobuf_int32 option;
      mutable segment: Onnx_piqi.tensor_proto_segment option;
      mutable float_data: Onnx_piqi.float32 list;
      mutable int32_data: Onnx_piqi.protobuf_int32 list;
      mutable string_data: Onnx_piqi.binary list;
      mutable int64_data: Onnx_piqi.protobuf_int64 list;
      mutable name: string option;
      mutable doc_string: string option;
      mutable raw_data: Onnx_piqi.binary option;
      mutable external_data: Onnx_piqi.string_string_entry_proto list;
      mutable data_location: Onnx_piqi.tensor_proto_data_location option;
      mutable double_data: Onnx_piqi.float64 list;
      mutable uint64_data: Onnx_piqi.uint64 list;
    }
  end = Tensor_proto
and Tensor_proto_segment:
  sig
    type t = {
      mutable begin_: Onnx_piqi.protobuf_int64 option;
      mutable end_: Onnx_piqi.protobuf_int64 option;
    }
  end = Tensor_proto_segment
and Tensor_shape_proto:
  sig
    type t = {
      mutable dim: Onnx_piqi.tensor_shape_proto_dimension list;
    }
  end = Tensor_shape_proto
and Tensor_shape_proto_dimension:
  sig
    type t = {
      mutable dim_value: Onnx_piqi.protobuf_int64 option;
      mutable dim_param: string option;
      mutable denotation: string option;
    }
  end = Tensor_shape_proto_dimension
and Type_proto:
  sig
    type t = {
      mutable tensor_type: Onnx_piqi.type_proto_tensor option;
      mutable denotation: string option;
    }
  end = Type_proto
and Type_proto_tensor:
  sig
    type t = {
      mutable elem_type: Onnx_piqi.protobuf_int32 option;
      mutable shape: Onnx_piqi.tensor_shape_proto option;
    }
  end = Type_proto_tensor
and Operator_set_id_proto:
  sig
    type t = {
      mutable domain: string option;
      mutable version: Onnx_piqi.protobuf_int64 option;
    }
  end = Operator_set_id_proto


let rec parse_int64 x = Piqirun.int64_of_zigzag_varint x
and packed_parse_int64 x = Piqirun.int64_of_packed_zigzag_varint x

and parse_int32 x = Piqirun.int32_of_zigzag_varint x
and packed_parse_int32 x = Piqirun.int32_of_packed_zigzag_varint x

and parse_string x = Piqirun.string_of_block x

and parse_float32 x = Piqirun.float_of_fixed32 x
and packed_parse_float32 x = Piqirun.float_of_packed_fixed32 x

and parse_protobuf_int64 x = Piqirun.int64_of_signed_varint x
and packed_parse_protobuf_int64 x = Piqirun.int64_of_packed_signed_varint x

and parse_binary x = Piqirun.string_of_block x

and parse_protobuf_int32 x = Piqirun.int32_of_signed_varint x
and packed_parse_protobuf_int32 x = Piqirun.int32_of_packed_signed_varint x

and parse_float64 x = Piqirun.float_of_fixed64 x
and packed_parse_float64 x = Piqirun.float_of_packed_fixed64 x

and parse_uint64 x = Piqirun.int64_of_varint x
and packed_parse_uint64 x = Piqirun.int64_of_packed_varint x

and parse_attribute_proto x =
  let x = Piqirun.parse_record x in
  let _name, x = Piqirun.parse_optional_field 1 parse_string x in
  let _f, x = Piqirun.parse_optional_field 2 parse_float32 x in
  let _i, x = Piqirun.parse_optional_field 3 parse_protobuf_int64 x in
  let _s, x = Piqirun.parse_optional_field 4 parse_binary x in
  let _t, x = Piqirun.parse_optional_field 5 parse_tensor_proto x in
  let _g, x = Piqirun.parse_optional_field 6 parse_graph_proto x in
  let _floats, x = Piqirun.parse_repeated_field 7 parse_float32 x in
  let _ints, x = Piqirun.parse_repeated_field 8 parse_protobuf_int64 x in
  let _strings, x = Piqirun.parse_repeated_field 9 parse_binary x in
  let _tensors, x = Piqirun.parse_repeated_field 10 parse_tensor_proto x in
  let _graphs, x = Piqirun.parse_repeated_field 11 parse_graph_proto x in
  let _doc_string, x = Piqirun.parse_optional_field 13 parse_string x in
  let _type_, x = Piqirun.parse_optional_field 20 parse_attribute_proto_attribute_type x in
  let _ref_attr_name, x = Piqirun.parse_optional_field 21 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    Attribute_proto.name = _name;
    Attribute_proto.f = _f;
    Attribute_proto.i = _i;
    Attribute_proto.s = _s;
    Attribute_proto.t = _t;
    Attribute_proto.g = _g;
    Attribute_proto.floats = _floats;
    Attribute_proto.ints = _ints;
    Attribute_proto.strings = _strings;
    Attribute_proto.tensors = _tensors;
    Attribute_proto.graphs = _graphs;
    Attribute_proto.doc_string = _doc_string;
    Attribute_proto.type_ = _type_;
    Attribute_proto.ref_attr_name = _ref_attr_name;
  }

and parse_attribute_proto_attribute_type x =
  match Piqirun.int32_of_signed_varint x with
    | 0l -> `undefined
    | 1l -> `float
    | 2l -> `int
    | 3l -> `string
    | 4l -> `tensor
    | 5l -> `graph
    | 6l -> `floats
    | 7l -> `ints
    | 8l -> `strings
    | 9l -> `tensors
    | 10l -> `graphs
    | x -> Piqirun.error_enum_const x
and packed_parse_attribute_proto_attribute_type x =
  match Piqirun.int32_of_packed_signed_varint x with
    | 0l -> `undefined
    | 1l -> `float
    | 2l -> `int
    | 3l -> `string
    | 4l -> `tensor
    | 5l -> `graph
    | 6l -> `floats
    | 7l -> `ints
    | 8l -> `strings
    | 9l -> `tensors
    | 10l -> `graphs
    | x -> Piqirun.error_enum_const x

and parse_value_info_proto x =
  let x = Piqirun.parse_record x in
  let _name, x = Piqirun.parse_optional_field 1 parse_string x in
  let _type_, x = Piqirun.parse_optional_field 2 parse_type_proto x in
  let _doc_string, x = Piqirun.parse_optional_field 3 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    Value_info_proto.name = _name;
    Value_info_proto.type_ = _type_;
    Value_info_proto.doc_string = _doc_string;
  }

and parse_node_proto x =
  let x = Piqirun.parse_record x in
  let _input, x = Piqirun.parse_repeated_field 1 parse_string x in
  let _output, x = Piqirun.parse_repeated_field 2 parse_string x in
  let _name, x = Piqirun.parse_optional_field 3 parse_string x in
  let _op_type, x = Piqirun.parse_optional_field 4 parse_string x in
  let _attribute, x = Piqirun.parse_repeated_field 5 parse_attribute_proto x in
  let _doc_string, x = Piqirun.parse_optional_field 6 parse_string x in
  let _domain, x = Piqirun.parse_optional_field 7 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    Node_proto.input = _input;
    Node_proto.output = _output;
    Node_proto.name = _name;
    Node_proto.op_type = _op_type;
    Node_proto.attribute = _attribute;
    Node_proto.doc_string = _doc_string;
    Node_proto.domain = _domain;
  }

and parse_model_proto x =
  let x = Piqirun.parse_record x in
  let _ir_version, x = Piqirun.parse_optional_field 1 parse_protobuf_int64 x in
  let _producer_name, x = Piqirun.parse_optional_field 2 parse_string x in
  let _producer_version, x = Piqirun.parse_optional_field 3 parse_string x in
  let _domain, x = Piqirun.parse_optional_field 4 parse_string x in
  let _model_version, x = Piqirun.parse_optional_field 5 parse_protobuf_int64 x in
  let _doc_string, x = Piqirun.parse_optional_field 6 parse_string x in
  let _graph, x = Piqirun.parse_optional_field 7 parse_graph_proto x in
  let _opset_import, x = Piqirun.parse_repeated_field 8 parse_operator_set_id_proto x in
  let _metadata_props, x = Piqirun.parse_repeated_field 14 parse_string_string_entry_proto x in
  Piqirun.check_unparsed_fields x;
  {
    Model_proto.ir_version = _ir_version;
    Model_proto.producer_name = _producer_name;
    Model_proto.producer_version = _producer_version;
    Model_proto.domain = _domain;
    Model_proto.model_version = _model_version;
    Model_proto.doc_string = _doc_string;
    Model_proto.graph = _graph;
    Model_proto.opset_import = _opset_import;
    Model_proto.metadata_props = _metadata_props;
  }

and parse_string_string_entry_proto x =
  let x = Piqirun.parse_record x in
  let _key, x = Piqirun.parse_optional_field 1 parse_string x in
  let _value, x = Piqirun.parse_optional_field 2 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    String_string_entry_proto.key = _key;
    String_string_entry_proto.value = _value;
  }

and parse_graph_proto x =
  let x = Piqirun.parse_record x in
  let _node, x = Piqirun.parse_repeated_field 1 parse_node_proto x in
  let _name, x = Piqirun.parse_optional_field 2 parse_string x in
  let _initializer_, x = Piqirun.parse_repeated_field 5 parse_tensor_proto x in
  let _doc_string, x = Piqirun.parse_optional_field 10 parse_string x in
  let _input, x = Piqirun.parse_repeated_field 11 parse_value_info_proto x in
  let _output, x = Piqirun.parse_repeated_field 12 parse_value_info_proto x in
  let _value_info, x = Piqirun.parse_repeated_field 13 parse_value_info_proto x in
  Piqirun.check_unparsed_fields x;
  {
    Graph_proto.node = _node;
    Graph_proto.name = _name;
    Graph_proto.initializer_ = _initializer_;
    Graph_proto.doc_string = _doc_string;
    Graph_proto.input = _input;
    Graph_proto.output = _output;
    Graph_proto.value_info = _value_info;
  }

and parse_tensor_proto x =
  let x = Piqirun.parse_record x in
  let _dims, x = Piqirun.parse_repeated_field 1 parse_protobuf_int64 x in
  let _data_type, x = Piqirun.parse_optional_field 2 parse_protobuf_int32 x in
  let _segment, x = Piqirun.parse_optional_field 3 parse_tensor_proto_segment x in
  let _float_data, x = Piqirun.parse_packed_repeated_field 4 packed_parse_float32 parse_float32 x in
  let _int32_data, x = Piqirun.parse_packed_repeated_field 5 packed_parse_protobuf_int32 parse_protobuf_int32 x in
  let _string_data, x = Piqirun.parse_repeated_field 6 parse_binary x in
  let _int64_data, x = Piqirun.parse_packed_repeated_field 7 packed_parse_protobuf_int64 parse_protobuf_int64 x in
  let _name, x = Piqirun.parse_optional_field 8 parse_string x in
  let _raw_data, x = Piqirun.parse_optional_field 9 parse_binary x in
  let _double_data, x = Piqirun.parse_packed_repeated_field 10 packed_parse_float64 parse_float64 x in
  let _uint64_data, x = Piqirun.parse_packed_repeated_field 11 packed_parse_uint64 parse_uint64 x in
  let _doc_string, x = Piqirun.parse_optional_field 12 parse_string x in
  let _external_data, x = Piqirun.parse_repeated_field 13 parse_string_string_entry_proto x in
  let _data_location, x = Piqirun.parse_optional_field 14 parse_tensor_proto_data_location x in
  Piqirun.check_unparsed_fields x;
  {
    Tensor_proto.dims = _dims;
    Tensor_proto.data_type = _data_type;
    Tensor_proto.segment = _segment;
    Tensor_proto.float_data = _float_data;
    Tensor_proto.int32_data = _int32_data;
    Tensor_proto.string_data = _string_data;
    Tensor_proto.int64_data = _int64_data;
    Tensor_proto.name = _name;
    Tensor_proto.raw_data = _raw_data;
    Tensor_proto.double_data = _double_data;
    Tensor_proto.uint64_data = _uint64_data;
    Tensor_proto.doc_string = _doc_string;
    Tensor_proto.external_data = _external_data;
    Tensor_proto.data_location = _data_location;
  }

and parse_tensor_proto_segment x =
  let x = Piqirun.parse_record x in
  let _begin_, x = Piqirun.parse_optional_field 1 parse_protobuf_int64 x in
  let _end_, x = Piqirun.parse_optional_field 2 parse_protobuf_int64 x in
  Piqirun.check_unparsed_fields x;
  {
    Tensor_proto_segment.begin_ = _begin_;
    Tensor_proto_segment.end_ = _end_;
  }

and parse_tensor_proto_data_type x =
  match Piqirun.int32_of_signed_varint x with
    | 0l -> `undefined
    | 1l -> `float
    | 2l -> `uint8
    | 3l -> `int8
    | 4l -> `uint16
    | 5l -> `int16
    | 6l -> `int32
    | 7l -> `int64
    | 8l -> `string
    | 9l -> `bool
    | 10l -> `float16
    | 11l -> `double
    | 12l -> `uint32
    | 13l -> `uint64
    | 14l -> `complex64
    | 15l -> `complex128
    | 16l -> `bfloat16
    | x -> Piqirun.error_enum_const x
and packed_parse_tensor_proto_data_type x =
  match Piqirun.int32_of_packed_signed_varint x with
    | 0l -> `undefined
    | 1l -> `float
    | 2l -> `uint8
    | 3l -> `int8
    | 4l -> `uint16
    | 5l -> `int16
    | 6l -> `int32
    | 7l -> `int64
    | 8l -> `string
    | 9l -> `bool
    | 10l -> `float16
    | 11l -> `double
    | 12l -> `uint32
    | 13l -> `uint64
    | 14l -> `complex64
    | 15l -> `complex128
    | 16l -> `bfloat16
    | x -> Piqirun.error_enum_const x

and parse_tensor_proto_data_location x =
  match Piqirun.int32_of_signed_varint x with
    | 0l -> `default
    | 1l -> `externall
    | x -> Piqirun.error_enum_const x
and packed_parse_tensor_proto_data_location x =
  match Piqirun.int32_of_packed_signed_varint x with
    | 0l -> `default
    | 1l -> `externall
    | x -> Piqirun.error_enum_const x

and parse_tensor_shape_proto x =
  let x = Piqirun.parse_record x in
  let _dim, x = Piqirun.parse_repeated_field 1 parse_tensor_shape_proto_dimension x in
  Piqirun.check_unparsed_fields x;
  {
    Tensor_shape_proto.dim = _dim;
  }

and parse_tensor_shape_proto_dimension x =
  let x = Piqirun.parse_record x in
  let _dim_value, x = Piqirun.parse_optional_field 1 parse_protobuf_int64 x in
  let _dim_param, x = Piqirun.parse_optional_field 2 parse_string x in
  let _denotation, x = Piqirun.parse_optional_field 3 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    Tensor_shape_proto_dimension.dim_value = _dim_value;
    Tensor_shape_proto_dimension.dim_param = _dim_param;
    Tensor_shape_proto_dimension.denotation = _denotation;
  }

and parse_type_proto x =
  let x = Piqirun.parse_record x in
  let _tensor_type, x = Piqirun.parse_optional_field 1 parse_type_proto_tensor x in
  let _denotation, x = Piqirun.parse_optional_field 6 parse_string x in
  Piqirun.check_unparsed_fields x;
  {
    Type_proto.tensor_type = _tensor_type;
    Type_proto.denotation = _denotation;
  }

and parse_type_proto_tensor x =
  let x = Piqirun.parse_record x in
  let _elem_type, x = Piqirun.parse_optional_field 1 parse_protobuf_int32 x in
  let _shape, x = Piqirun.parse_optional_field 2 parse_tensor_shape_proto x in
  Piqirun.check_unparsed_fields x;
  {
    Type_proto_tensor.elem_type = _elem_type;
    Type_proto_tensor.shape = _shape;
  }

and parse_operator_set_id_proto x =
  let x = Piqirun.parse_record x in
  let _domain, x = Piqirun.parse_optional_field 1 parse_string x in
  let _version, x = Piqirun.parse_optional_field 2 parse_protobuf_int64 x in
  Piqirun.check_unparsed_fields x;
  {
    Operator_set_id_proto.domain = _domain;
    Operator_set_id_proto.version = _version;
  }

and parse_version x =
  match Piqirun.int32_of_signed_varint x with
    | 0l -> `start_version
    | 1l -> `ir_version_2017_10_10
    | 2l -> `ir_version_2017_10_30
    | 3l -> `ir_version_2017_11_3
    | 4l -> `ir_version
    | x -> Piqirun.error_enum_const x
and packed_parse_version x =
  match Piqirun.int32_of_packed_signed_varint x with
    | 0l -> `start_version
    | 1l -> `ir_version_2017_10_10
    | 2l -> `ir_version_2017_10_30
    | 3l -> `ir_version_2017_11_3
    | 4l -> `ir_version
    | x -> Piqirun.error_enum_const x


let rec gen__int64 code x = Piqirun.int64_to_zigzag_varint code x
and packed_gen__int64 x = Piqirun.int64_to_packed_zigzag_varint x

and gen__int32 code x = Piqirun.int32_to_zigzag_varint code x
and packed_gen__int32 x = Piqirun.int32_to_packed_zigzag_varint x

and gen__string code x = Piqirun.string_to_block code x

and gen__float32 code x = Piqirun.float_to_fixed32 code x
and packed_gen__float32 x = Piqirun.float_to_packed_fixed32 x

and gen__protobuf_int64 code x = Piqirun.int64_to_signed_varint code x
and packed_gen__protobuf_int64 x = Piqirun.int64_to_packed_signed_varint x

and gen__binary code x = Piqirun.string_to_block code x

and gen__protobuf_int32 code x = Piqirun.int32_to_signed_varint code x
and packed_gen__protobuf_int32 x = Piqirun.int32_to_packed_signed_varint x

and gen__float64 code x = Piqirun.float_to_fixed64 code x
and packed_gen__float64 x = Piqirun.float_to_packed_fixed64 x

and gen__uint64 code x = Piqirun.int64_to_varint code x
and packed_gen__uint64 x = Piqirun.int64_to_packed_varint x

and gen__attribute_proto code x =
  let _name = Piqirun.gen_optional_field 1 gen__string x.Attribute_proto.name in
  let _f = Piqirun.gen_optional_field 2 gen__float32 x.Attribute_proto.f in
  let _i = Piqirun.gen_optional_field 3 gen__protobuf_int64 x.Attribute_proto.i in
  let _s = Piqirun.gen_optional_field 4 gen__binary x.Attribute_proto.s in
  let _t = Piqirun.gen_optional_field 5 gen__tensor_proto x.Attribute_proto.t in
  let _g = Piqirun.gen_optional_field 6 gen__graph_proto x.Attribute_proto.g in
  let _floats = Piqirun.gen_repeated_field 7 gen__float32 x.Attribute_proto.floats in
  let _ints = Piqirun.gen_repeated_field 8 gen__protobuf_int64 x.Attribute_proto.ints in
  let _strings = Piqirun.gen_repeated_field 9 gen__binary x.Attribute_proto.strings in
  let _tensors = Piqirun.gen_repeated_field 10 gen__tensor_proto x.Attribute_proto.tensors in
  let _graphs = Piqirun.gen_repeated_field 11 gen__graph_proto x.Attribute_proto.graphs in
  let _doc_string = Piqirun.gen_optional_field 13 gen__string x.Attribute_proto.doc_string in
  let _type_ = Piqirun.gen_optional_field 20 gen__attribute_proto_attribute_type x.Attribute_proto.type_ in
  let _ref_attr_name = Piqirun.gen_optional_field 21 gen__string x.Attribute_proto.ref_attr_name in
  Piqirun.gen_record code (_name :: _f :: _i :: _s :: _t :: _g :: _floats :: _ints :: _strings :: _tensors :: _graphs :: _doc_string :: _type_ :: _ref_attr_name :: [])

and gen__attribute_proto_attribute_type code x =
  Piqirun.int32_to_signed_varint code (match x with
    | `undefined -> 0l
    | `float -> 1l
    | `int -> 2l
    | `string -> 3l
    | `tensor -> 4l
    | `graph -> 5l
    | `floats -> 6l
    | `ints -> 7l
    | `strings -> 8l
    | `tensors -> 9l
    | `graphs -> 10l
  )
and packed_gen__attribute_proto_attribute_type x =
  Piqirun.int32_to_packed_signed_varint (match x with
    | `undefined -> 0l
    | `float -> 1l
    | `int -> 2l
    | `string -> 3l
    | `tensor -> 4l
    | `graph -> 5l
    | `floats -> 6l
    | `ints -> 7l
    | `strings -> 8l
    | `tensors -> 9l
    | `graphs -> 10l
  )

and gen__value_info_proto code x =
  let _name = Piqirun.gen_optional_field 1 gen__string x.Value_info_proto.name in
  let _type_ = Piqirun.gen_optional_field 2 gen__type_proto x.Value_info_proto.type_ in
  let _doc_string = Piqirun.gen_optional_field 3 gen__string x.Value_info_proto.doc_string in
  Piqirun.gen_record code (_name :: _type_ :: _doc_string :: [])

and gen__node_proto code x =
  let _input = Piqirun.gen_repeated_field 1 gen__string x.Node_proto.input in
  let _output = Piqirun.gen_repeated_field 2 gen__string x.Node_proto.output in
  let _name = Piqirun.gen_optional_field 3 gen__string x.Node_proto.name in
  let _op_type = Piqirun.gen_optional_field 4 gen__string x.Node_proto.op_type in
  let _attribute = Piqirun.gen_repeated_field 5 gen__attribute_proto x.Node_proto.attribute in
  let _doc_string = Piqirun.gen_optional_field 6 gen__string x.Node_proto.doc_string in
  let _domain = Piqirun.gen_optional_field 7 gen__string x.Node_proto.domain in
  Piqirun.gen_record code (_input :: _output :: _name :: _op_type :: _attribute :: _doc_string :: _domain :: [])

and gen__model_proto code x =
  let _ir_version = Piqirun.gen_optional_field 1 gen__protobuf_int64 x.Model_proto.ir_version in
  let _producer_name = Piqirun.gen_optional_field 2 gen__string x.Model_proto.producer_name in
  let _producer_version = Piqirun.gen_optional_field 3 gen__string x.Model_proto.producer_version in
  let _domain = Piqirun.gen_optional_field 4 gen__string x.Model_proto.domain in
  let _model_version = Piqirun.gen_optional_field 5 gen__protobuf_int64 x.Model_proto.model_version in
  let _doc_string = Piqirun.gen_optional_field 6 gen__string x.Model_proto.doc_string in
  let _graph = Piqirun.gen_optional_field 7 gen__graph_proto x.Model_proto.graph in
  let _opset_import = Piqirun.gen_repeated_field 8 gen__operator_set_id_proto x.Model_proto.opset_import in
  let _metadata_props = Piqirun.gen_repeated_field 14 gen__string_string_entry_proto x.Model_proto.metadata_props in
  Piqirun.gen_record code (_ir_version :: _producer_name :: _producer_version :: _domain :: _model_version :: _doc_string :: _graph :: _opset_import :: _metadata_props :: [])

and gen__string_string_entry_proto code x =
  let _key = Piqirun.gen_optional_field 1 gen__string x.String_string_entry_proto.key in
  let _value = Piqirun.gen_optional_field 2 gen__string x.String_string_entry_proto.value in
  Piqirun.gen_record code (_key :: _value :: [])

and gen__graph_proto code x =
  let _node = Piqirun.gen_repeated_field 1 gen__node_proto x.Graph_proto.node in
  let _name = Piqirun.gen_optional_field 2 gen__string x.Graph_proto.name in
  let _initializer_ = Piqirun.gen_repeated_field 5 gen__tensor_proto x.Graph_proto.initializer_ in
  let _doc_string = Piqirun.gen_optional_field 10 gen__string x.Graph_proto.doc_string in
  let _input = Piqirun.gen_repeated_field 11 gen__value_info_proto x.Graph_proto.input in
  let _output = Piqirun.gen_repeated_field 12 gen__value_info_proto x.Graph_proto.output in
  let _value_info = Piqirun.gen_repeated_field 13 gen__value_info_proto x.Graph_proto.value_info in
  Piqirun.gen_record code (_node :: _name :: _initializer_ :: _doc_string :: _input :: _output :: _value_info :: [])

and gen__tensor_proto code x =
  let _dims = Piqirun.gen_repeated_field 1 gen__protobuf_int64 x.Tensor_proto.dims in
  let _data_type = Piqirun.gen_optional_field 2 gen__protobuf_int32 x.Tensor_proto.data_type in
  let _segment = Piqirun.gen_optional_field 3 gen__tensor_proto_segment x.Tensor_proto.segment in
  let _float_data = Piqirun.gen_packed_repeated_field 4 packed_gen__float32 x.Tensor_proto.float_data in
  let _int32_data = Piqirun.gen_packed_repeated_field 5 packed_gen__protobuf_int32 x.Tensor_proto.int32_data in
  let _string_data = Piqirun.gen_repeated_field 6 gen__binary x.Tensor_proto.string_data in
  let _int64_data = Piqirun.gen_packed_repeated_field 7 packed_gen__protobuf_int64 x.Tensor_proto.int64_data in
  let _name = Piqirun.gen_optional_field 8 gen__string x.Tensor_proto.name in
  let _raw_data = Piqirun.gen_optional_field 9 gen__binary x.Tensor_proto.raw_data in
  let _double_data = Piqirun.gen_packed_repeated_field 10 packed_gen__float64 x.Tensor_proto.double_data in
  let _uint64_data = Piqirun.gen_packed_repeated_field 11 packed_gen__uint64 x.Tensor_proto.uint64_data in
  let _doc_string = Piqirun.gen_optional_field 12 gen__string x.Tensor_proto.doc_string in
  let _external_data = Piqirun.gen_repeated_field 13 gen__string_string_entry_proto x.Tensor_proto.external_data in
  let _data_location = Piqirun.gen_optional_field 14 gen__tensor_proto_data_location x.Tensor_proto.data_location in
  Piqirun.gen_record code (_dims :: _data_type :: _segment :: _float_data :: _int32_data :: _string_data :: _int64_data :: _name :: _raw_data :: _double_data :: _uint64_data :: _doc_string :: _external_data :: _data_location :: [])

and gen__tensor_proto_segment code x =
  let _begin_ = Piqirun.gen_optional_field 1 gen__protobuf_int64 x.Tensor_proto_segment.begin_ in
  let _end_ = Piqirun.gen_optional_field 2 gen__protobuf_int64 x.Tensor_proto_segment.end_ in
  Piqirun.gen_record code (_begin_ :: _end_ :: [])

and gen__tensor_proto_data_type code x =
  Piqirun.int32_to_signed_varint code (match x with
    | `undefined -> 0l
    | `float -> 1l
    | `uint8 -> 2l
    | `int8 -> 3l
    | `uint16 -> 4l
    | `int16 -> 5l
    | `int32 -> 6l
    | `int64 -> 7l
    | `string -> 8l
    | `bool -> 9l
    | `float16 -> 10l
    | `double -> 11l
    | `uint32 -> 12l
    | `uint64 -> 13l
    | `complex64 -> 14l
    | `complex128 -> 15l
    | `bfloat16 -> 16l
  )
and packed_gen__tensor_proto_data_type x =
  Piqirun.int32_to_packed_signed_varint (match x with
    | `undefined -> 0l
    | `float -> 1l
    | `uint8 -> 2l
    | `int8 -> 3l
    | `uint16 -> 4l
    | `int16 -> 5l
    | `int32 -> 6l
    | `int64 -> 7l
    | `string -> 8l
    | `bool -> 9l
    | `float16 -> 10l
    | `double -> 11l
    | `uint32 -> 12l
    | `uint64 -> 13l
    | `complex64 -> 14l
    | `complex128 -> 15l
    | `bfloat16 -> 16l
  )

and gen__tensor_proto_data_location code x =
  Piqirun.int32_to_signed_varint code (match x with
    | `default -> 0l
    | `externall -> 1l
  )
and packed_gen__tensor_proto_data_location x =
  Piqirun.int32_to_packed_signed_varint (match x with
    | `default -> 0l
    | `externall -> 1l
  )

and gen__tensor_shape_proto code x =
  let _dim = Piqirun.gen_repeated_field 1 gen__tensor_shape_proto_dimension x.Tensor_shape_proto.dim in
  Piqirun.gen_record code (_dim :: [])

and gen__tensor_shape_proto_dimension code x =
  let _dim_value = Piqirun.gen_optional_field 1 gen__protobuf_int64 x.Tensor_shape_proto_dimension.dim_value in
  let _dim_param = Piqirun.gen_optional_field 2 gen__string x.Tensor_shape_proto_dimension.dim_param in
  let _denotation = Piqirun.gen_optional_field 3 gen__string x.Tensor_shape_proto_dimension.denotation in
  Piqirun.gen_record code (_dim_value :: _dim_param :: _denotation :: [])

and gen__type_proto code x =
  let _tensor_type = Piqirun.gen_optional_field 1 gen__type_proto_tensor x.Type_proto.tensor_type in
  let _denotation = Piqirun.gen_optional_field 6 gen__string x.Type_proto.denotation in
  Piqirun.gen_record code (_tensor_type :: _denotation :: [])

and gen__type_proto_tensor code x =
  let _elem_type = Piqirun.gen_optional_field 1 gen__protobuf_int32 x.Type_proto_tensor.elem_type in
  let _shape = Piqirun.gen_optional_field 2 gen__tensor_shape_proto x.Type_proto_tensor.shape in
  Piqirun.gen_record code (_elem_type :: _shape :: [])

and gen__operator_set_id_proto code x =
  let _domain = Piqirun.gen_optional_field 1 gen__string x.Operator_set_id_proto.domain in
  let _version = Piqirun.gen_optional_field 2 gen__protobuf_int64 x.Operator_set_id_proto.version in
  Piqirun.gen_record code (_domain :: _version :: [])

and gen__version code x =
  Piqirun.int32_to_signed_varint code (match x with
    | `start_version -> 0l
    | `ir_version_2017_10_10 -> 1l
    | `ir_version_2017_10_30 -> 2l
    | `ir_version_2017_11_3 -> 3l
    | `ir_version -> 4l
  )
and packed_gen__version x =
  Piqirun.int32_to_packed_signed_varint (match x with
    | `start_version -> 0l
    | `ir_version_2017_10_10 -> 1l
    | `ir_version_2017_10_30 -> 2l
    | `ir_version_2017_11_3 -> 3l
    | `ir_version -> 4l
  )


let gen_int64 x = gen__int64 (-1) x
let gen_int32 x = gen__int32 (-1) x
let gen_string x = gen__string (-1) x
let gen_float32 x = gen__float32 (-1) x
let gen_protobuf_int64 x = gen__protobuf_int64 (-1) x
let gen_binary x = gen__binary (-1) x
let gen_protobuf_int32 x = gen__protobuf_int32 (-1) x
let gen_float64 x = gen__float64 (-1) x
let gen_uint64 x = gen__uint64 (-1) x
let gen_attribute_proto x = gen__attribute_proto (-1) x
let gen_attribute_proto_attribute_type x = gen__attribute_proto_attribute_type (-1) x
let gen_value_info_proto x = gen__value_info_proto (-1) x
let gen_node_proto x = gen__node_proto (-1) x
let gen_model_proto x = gen__model_proto (-1) x
let gen_string_string_entry_proto x = gen__string_string_entry_proto (-1) x
let gen_graph_proto x = gen__graph_proto (-1) x
let gen_tensor_proto x = gen__tensor_proto (-1) x
let gen_tensor_proto_segment x = gen__tensor_proto_segment (-1) x
let gen_tensor_proto_data_type x = gen__tensor_proto_data_type (-1) x
let gen_tensor_proto_data_location x = gen__tensor_proto_data_location (-1) x
let gen_tensor_shape_proto x = gen__tensor_shape_proto (-1) x
let gen_tensor_shape_proto_dimension x = gen__tensor_shape_proto_dimension (-1) x
let gen_type_proto x = gen__type_proto (-1) x
let gen_type_proto_tensor x = gen__type_proto_tensor (-1) x
let gen_operator_set_id_proto x = gen__operator_set_id_proto (-1) x
let gen_version x = gen__version (-1) x


let rec default_int64 () = 0L
and default_int32 () = 0l
and default_string () = ""
and default_float32 () = 0.0
and default_protobuf_int64 () = default_int64 ()
and default_binary () = ""
and default_protobuf_int32 () = default_int32 ()
and default_float64 () = 0.0
and default_uint64 () = 0L
and default_attribute_proto () =
  {
    Attribute_proto.name = None;
    Attribute_proto.f = None;
    Attribute_proto.i = None;
    Attribute_proto.s = None;
    Attribute_proto.t = None;
    Attribute_proto.g = None;
    Attribute_proto.floats = [];
    Attribute_proto.ints = [];
    Attribute_proto.strings = [];
    Attribute_proto.tensors = [];
    Attribute_proto.graphs = [];
    Attribute_proto.doc_string = None;
    Attribute_proto.type_ = None;
    Attribute_proto.ref_attr_name = None;
  }
and default_attribute_proto_attribute_type () = `undefined
and default_value_info_proto () =
  {
    Value_info_proto.name = None;
    Value_info_proto.type_ = None;
    Value_info_proto.doc_string = None;
  }
and default_node_proto () =
  {
    Node_proto.input = [];
    Node_proto.output = [];
    Node_proto.name = None;
    Node_proto.op_type = None;
    Node_proto.attribute = [];
    Node_proto.doc_string = None;
    Node_proto.domain = None;
  }
and default_model_proto () =
  {
    Model_proto.ir_version = None;
    Model_proto.producer_name = None;
    Model_proto.producer_version = None;
    Model_proto.domain = None;
    Model_proto.model_version = None;
    Model_proto.doc_string = None;
    Model_proto.graph = None;
    Model_proto.opset_import = [];
    Model_proto.metadata_props = [];
  }
and default_string_string_entry_proto () =
  {
    String_string_entry_proto.key = None;
    String_string_entry_proto.value = None;
  }
and default_graph_proto () =
  {
    Graph_proto.node = [];
    Graph_proto.name = None;
    Graph_proto.initializer_ = [];
    Graph_proto.doc_string = None;
    Graph_proto.input = [];
    Graph_proto.output = [];
    Graph_proto.value_info = [];
  }
and default_tensor_proto () =
  {
    Tensor_proto.dims = [];
    Tensor_proto.data_type = None;
    Tensor_proto.segment = None;
    Tensor_proto.float_data = [];
    Tensor_proto.int32_data = [];
    Tensor_proto.string_data = [];
    Tensor_proto.int64_data = [];
    Tensor_proto.name = None;
    Tensor_proto.raw_data = None;
    Tensor_proto.double_data = [];
    Tensor_proto.uint64_data = [];
    Tensor_proto.doc_string = None;
    Tensor_proto.external_data = [];
    Tensor_proto.data_location = None;
  }
and default_tensor_proto_segment () =
  {
    Tensor_proto_segment.begin_ = None;
    Tensor_proto_segment.end_ = None;
  }
and default_tensor_proto_data_type () = `undefined
and default_tensor_proto_data_location () = `default
and default_tensor_shape_proto () =
  {
    Tensor_shape_proto.dim = [];
  }
and default_tensor_shape_proto_dimension () =
  {
    Tensor_shape_proto_dimension.dim_value = None;
    Tensor_shape_proto_dimension.dim_param = None;
    Tensor_shape_proto_dimension.denotation = None;
  }
and default_type_proto () =
  {
    Type_proto.tensor_type = None;
    Type_proto.denotation = None;
  }
and default_type_proto_tensor () =
  {
    Type_proto_tensor.elem_type = None;
    Type_proto_tensor.shape = None;
  }
and default_operator_set_id_proto () =
  {
    Operator_set_id_proto.domain = None;
    Operator_set_id_proto.version = None;
  }
and default_version () = `start_version


include Onnx_piqi
