(** This module defines a control flow graph tailored
    for ONNX models, NIER-CFG.

    It is primarly designed as an intermediate
    state into producing verifiable
    terms from an ONNX model.

    NIER-CFG is meant to be mutable, for various rewriting
    techniques to be easily used.

    To use this module, import module NierCFG to provide
    definitions and functions for the CFG.

    A visual representation of the graph can be saved under
    the dot graphviz
    format with the following:
    {[
      let file = open_out_bin "graph.dot"
      let () =  M.NierCFGDot.output_graph file graph
    ]}

    @author Zakaria Chihani, Julien Girard
*)

(*TODO:
 * take out operators outside of the Vertex module
 * provide basic API calls that are distincts from OCamlgraph*)

open Base


(** {1 Tensor module} *)

(** Tensors are defined to easily represent the
    stored numerical parameters *)

module Tensor : sig
  type t_val = float
  type 'a data = Row of 'a array | Dim of 'a data array
  [@@deriving show, iter, map, fold]
  type fdata = float data
  [@@deriving show]
  type shape = int list
  [@@deriving show]
  type t = {data:fdata; shape:shape}
  [@@deriving show]

  (** [create sh] initialize a tensor with the given
      shape [sh] with a default value,
      depending of the type of the tensor*)

  val create : shape -> t

  (** [get_idx t idx] returns the value in tensor [t]
      stored at coordinates
      [idx]. Throw an error if the coordinate is invalid.*)

  val get_idx : t -> shape -> t_val

  (** [set_idx t idx v] returns a tensor identical
      to tensor [t], except
      with the value stored at coordinates [idx];
      the new tensor has this value
      replaced by [v].
      Throw an error if the coordinate is invalid.*)

  val set_idx : t -> shape -> t_val -> t

  (** [all_rows t] returns a list of all Row within [t]
  *)

  val all_rows : t -> fdata list

  (** [num_neurons sh] returns the total number of neurons
      given a shape
  *)

  val num_neurons : shape -> int

  (** [all_coords sh] produces the list of all possible
   * coordinates within a shape [sh] *)

  val all_coords : shape -> shape list

  (** [get flatnd_idx idx sh flt] returns the value that would be stored at
      index [idx] under a tensor of shape [sh], given the flattened version
      of this tensor [flt].*)

  val get_flatnd_idx: idx:shape -> sh:shape -> 'a list -> 'a

  (** [get_row_at_idx idx t] returns the row at [idx] of the tensor [t].
      [idx] must either be a shape of length n-1 where n is the length of
      [t]'s shape.*)

  val get_row_at_idx: idx:shape -> t -> t_val data

  (** [transpose_2d t] returns the tensor [t] with its two last dimension
      exchanged. *)

  val transpose_2d: t -> t

  (** [unsqueeze sh1 sh2] returns the lowest common shape between
      [sh1] and [sh2], and None if there is no common shape. A common shape
      is when a shape of higher dimension has only 1 coordinates on non-shared
      dimensions with the other. *)

  val unsqueeze: sh1:shape -> sh2:shape -> shape option
end

(** {1 Modules for graph generation} *)

module Vertex : sig
  type id = int
  type shape = int list
  type operator =
    | Add
    | Mul
    | Matmul
    | LogSoftmax
    | ReLu
    | Transpose
    | Squeeze
    | MaxPool
    | Conv
    | Identity
    | NO_OP
    | RW_Linearized_ReLu

  (** Type describing the different operations handled.
      Those operations are defined in the ONNX documentation.
      @see <https://github.com/onnx/onnx/blob/master/docs/Operators.md> for more
      informations.
      They are to be coupled with the relevant operators
      parameters.
  *)

  val str_op : operator -> string
  val str_shape : shape -> string
  type ksize = Ksize of shape
  type stride = Stride of shape
  type pads = Pads of shape
  type dilations = Dilations of shape
  type operator_parameters =
    | Pool_params of (ksize*stride option*pads option*dilations option)
    | Conv_params of (ksize*stride option*pads option*dilations option)
    | Transpose_params of shape
    | RW_Linearized_ReLu_params of ((bool list list) * ((string, float) Base.Hashtbl.t list * int))

  val str_op_params : operator_parameters -> string

  (** Type encapsulating parameters for operations.
      For Convolutions and Pooling, kernel size, padding,
      strides
      For Transpose, shape
  *)
  type t = {
    id: id;
    name: string option;
    mutable shape: shape;
    operator: operator;
    operator_parameters: operator_parameters option;
    pred : string list;
    succ : string list;
    tensor : Tensor.t option
  }
  val compare : t -> t -> int
  val hash : t -> int
  val equal : t -> t -> bool
  val create : id:id -> name:string option -> sh:shape ->
    op:operator -> op_p:operator_parameters option ->
    pred:string list -> succ:string list ->
    tensor:Tensor.t option -> t
  val get_shape :  t -> shape
  val get_op :  t -> operator
  val get_tensor : t -> Tensor.t option
  val is_data_node : t -> bool
  val num_neurons : t -> int
end
module Edge :
sig
  type t = string
  val compare : 'a -> 'a -> int
  val equal : 'a -> 'a -> bool
  val default : t
end

(**  NIER-CFG is a graph {b (V,E)}
     where {b V} is the set of vertices (nodes)
     and {b E} is the set of edges
     (connections between nodes).
     Nodes contains the following informations:
     - unique id
     - name coming from the original model, if it exists
     - shape of the tensor resulting from the application of
       the node operation, if it exist
     - operation performed
     - parameters of the operation
     - an optional tensor storing the data

     Note that tensor have their own shape;
     they should not be different from
     the NierCFG node shape however.
*)

module NierCFG : Graph.Sig.I with
  type V.t = Vertex.t and type V.label = Vertex.t
                      and type E.t = Vertex.t * Edge.t * Vertex.t
                      and type E.label = Edge.t

val init_cfg : NierCFG.t
val vertex_list : NierCFG.t -> Vertex.t list

val preds : NierCFG.t -> Vertex.t -> Vertex.t list

(** [preds_names g v] returns a list of names of predecessors nodes
*)
val preds_names : NierCFG.t -> Vertex.t -> string list

val succs : NierCFG.t -> Vertex.t -> Vertex.t list

(** [succs_names g v] returns a list of names of predecessors nodes
*)
val succs_names : NierCFG.t -> Vertex.t -> string list


(** [input_node g] returns the nodes considered as describing the inputs
    of the neural network.
*)
val input_nodes : NierCFG.t -> Vertex.t list


(** {1 Pretty printers} *)

val print_cfg_graph : NierCFG.t -> unit
val out_cfg_graph : NierCFG.t -> unit
val find_vertices : NierCFG.t -> (Vertex.t -> bool) -> Vertex.t list
