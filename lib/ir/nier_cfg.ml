open Base
open Stdio

(*quick and dirty replacement for head and tail
 * to adapt to Base behaviour*)
let hd_id l = match List.hd l with
  | Some a -> a
  | None -> failwith "error, index is empty"
let tl_id l = match List.tl l with
  | Some a -> a
  | None -> failwith "error, index is empty"

module Tensor = struct
  type t_val = float
  (* one row: one dimension of value
   * dim: a dimension in the tensor*)
  type 'a data = Row of 'a array | Dim of 'a data array
  [@@deriving iter, map, fold, show]
  type fdata = float data
  [@@deriving show]
  type shape = int list
  [@@deriving show]
  type t = {data:fdata;shape:shape}
  [@@deriving show]
  (* start by creating the first dimension, then build arrays over*)
  let create shape = let rec create_ sh acc b = match sh with
      | x::y -> if b
        then create_ y (Row (Array.init x ~f:(fun _ -> 0.0))) false
        else create_ y (Dim (Array.init x ~f:(fun _ -> acc))) false
      | [] -> {data=acc;shape=shape}
    in create_ (List.rev shape) (Dim [||]) true
  let unsqueeze ~sh1 ~sh2 =
    let longest, shortest = match (List.length sh1) > (List.length sh2) with
      | true -> sh1, sh2
      | false -> sh2, sh1
    in
    (*find the index of the potential additional dimension*)
    (* printf "%s" (show_shape longest); *)
    (* printf "%s" (show_shape shortest); *)
    let where_zero = match List.nth_exn longest 0 with
      | 0 -> Some 0
      | _ -> (match List.last_exn longest with
          | 0 -> Some ((List.length longest)-1)
          | _ -> None)
    in match where_zero with
    | Some idx ->(match List.sub longest ~pos:idx
                          ~len:(List.length shortest) with
                 | [] -> None
                 | _ -> Some longest)
    | None -> None
  let get_idx t idx =
    (* printf "\nLength of shape: %d" (List.length t.shape); *)
    (* printf "\nshape of tensor: %s" (show_shape t.shape); *)
    (* printf "\nidx: %s\n" (show_shape idx); *)
    let true_idx = match List.length t.shape = List.length idx with
      | true -> idx
      | false -> (match unsqueeze ~sh1:idx ~sh2:t.shape with
          | Some shape -> shape
          | None -> failwith "error, index is too long for tensor shape")
    in
    (* printf "\ntrue idx: %s\n" (show_shape true_idx); *)
    let rec get_idx_ tdata id = match tdata with
      | Dim x -> get_idx_ x.(hd_id id) (tl_id id)
      | Row x -> x.(List.hd_exn id)
    in get_idx_ t.data true_idx

  let set_idx t (id:shape) v =
    if List.length t.shape <> List.length id then
      failwith "error, index is too long for tensor shape"
    else let rec new_row_ td (id:shape) v = match td with
        (*find the row corresponding to index*)
        | Dim x -> new_row_ x.(hd_id id) (tl_id id) v
        | Row x ->
          let idx = hd_id id in
          let new_row = Array.init
              (Array.length x)
              ~f:(fun i -> if i <> idx then x.(i) else v)
          in
          (* Array.iter (fun i -> Printf.printf "array: %f" i) new_row; *)
          Row new_row
      in
      let new_row = new_row_ t.data id v
      in
      (* Printf.printf "new shape incoming!\n"; *)
      (* Printf.printf "result of compare: %d\n" *)
      (* (compare (List.length t.shape) 2); *)
      let rec new_data_ sh acc nrow =
        (* List.iter (Printf.printf "%d\n") sh; *)
        match (compare (List.length sh) 2) with
        | 0  -> let Dim row_array = acc in
          Dim (Array.init (Array.length row_array)
                 (fun i -> if i <> (hd_id sh)
                   then row_array.(i) else nrow))
        | 1 -> let Dim dim_array = acc in
          let dim_to_explore = dim_array.(hd_id sh) in
          Dim (Array.init (Array.length dim_array)
                 (fun i -> if i <> (hd_id sh)
                   then dim_array.(i)
                   else new_data_ (tl_id sh) dim_to_explore nrow ))
        | -1  -> nrow
      in {data=(new_data_ id t.data new_row);shape=t.shape}
  let all_coords sh =
    let rec ranges acc shape = match shape with
      | x::y -> ranges ((List.init x ~f:(fun i -> i))::acc) y
      | [] -> acc
      (* a list containing a list of all possible indexes, for each dimension *)
    in let xxs = ranges [] sh in
    (* add to each element of the list of all possible coordinates all
     * possible indexes ... *)
    let aux acc xs = List.concat @@
      List.map xs ~f:(fun x ->
          List.map ~f:(fun lt -> x::lt) acc)
      (* ... for each dimension, starting from an empty list of
       * possible coordinates *)
    in List.fold_left xxs ~init:[[]] ~f:aux
  let get_shape t = t.shape
  let equal f t1 t2 =
    let t1_sh = get_shape t1 and t2_sh = get_shape t2 in
    if List.equal (=) t1_sh t2_sh then
      let all_idxs = all_coords (get_shape t1) in
      List.fold ~f:(fun acc x -> if acc
                     then f (get_idx t1 x) (get_idx t2 x)
                     else false) all_idxs ~init:true
    else
      false

  let all_rows t =
    let rec aux t_data l = match t_data with
      | Dim a -> begin
          match a.(0) with
          (*if Dim yields another Dim,
           * recursively apply aux*)
          | Dim _ -> List.concat @@
            Array.to_list(Array.map ~f:(fun x -> aux x l) a)
          (*if Dim yields Rows,
           * add all Rows to the list*)
          | Row _ ->
            Array.fold ~init:[] ~f:(fun l x -> x::l) a
        end
      | Row r -> (Row r)::l
    in aux t.data []
  let num_neurons sh = List.fold ~init:1
      ~f:(fun x y -> x*y) sh

  (*[get flatnd_idx idx sh flt] returns the value that would be stored at
   * index [idx] under a tensor of shape [sh], given the flattened version
   * of this tensor [flt].*)
  let get_flatnd_idx ~idx ~sh flt =
    let pop_sh = (List.tl_exn sh)@[1]
    in
    (* Returns the factors by which multiply each coordinate *)
    let rec get_factors_from_sh sh_f l = match sh_f with
      | [] -> List.rev l
      | _ -> get_factors_from_sh (List.tl_exn sh_f)
               ((List.fold ~f:(fun x y -> x*y) ~init:1 sh_f)::l)
    in
    let factors = get_factors_from_sh pop_sh [] in
    let coord_in_data =
      match List.fold2 ~f:(fun x y z -> x + y*z) ~init:0 idx factors with
      | List.Or_unequal_lengths.Ok i -> i
      | List.Or_unequal_lengths.Unequal_lengths -> failwith "Unequal lengths"
    in List.nth_exn flt coord_in_data

  (*[get_row_at_idx idx t] returns the row at [idx] of the N-D tensor [t],
   * where N >=2. [idx] must be a shape of length N-1 *)
  let get_row_at_idx ~idx t =
    match get_shape t with
    | [] -> failwith "Cannot take a row from an empty tensor"
    | [x]-> failwith ("Cannot take a row from 1-D tensor of shape
    "^(show_shape [x]))
    | l -> if (List.length idx) = ((List.length l) - 1) then
        (*Access to the dimensions*)
        let rec aux tdata acc = match tdata with
          | Dim x -> aux x.(List.hd_exn acc) (List.tl_exn acc)
          | Row x -> Row x
        in aux t.data idx
      else
        failwith "Error, provided index must have one less dimension than the
        required tensor"
  (**[transpose_2d t] returns the tensor [t] with its two last dimension
   * exchanged*)
  let transpose_2d t =
    let permute_two_last l =
      let last = List.last_exn l
      and second_to_last = List.nth_exn (List.rev l) 1
      in
      (List.rev (List.drop (List.rev l) 2 ))@[last]@[second_to_last]
    in
    let old_sh = get_shape t in
    let new_sh = permute_two_last old_sh in
    let new_coords = all_coords new_sh in
    {data=(
        List.fold new_coords ~init:(create new_sh)
          ~f:(fun acc new_idx ->
              set_idx acc new_idx
                (get_idx t (permute_two_last new_idx)))).data;
     shape=new_sh}
  (* Unit tests *)
  let%test_module "Test Tensor" =
    (module struct
      let sh = [3;2];;
      let t = create sh;;
      let t_copy = create sh;;
      let idx = [2;1];;
      let v = 5.0;;
      let t_n = set_idx t idx v;;
      let t_t = transpose_2d t_n;;
      let%test_unit "Printing tensor" = printf "%s\n" (show t_n)
      let%test_unit "Printing transpoed tensor" = printf "%s\n" (show t_t)
      let%test_unit "Printing get_row_at_idx" = printf "%s\n"
          (show {shape=[2;];data=(get_row_at_idx ~idx:[1] t_n)})
      let%test "Same tensors" = equal (Float.equal) t t_copy ;;
      let%test "Different tensors" = not (equal (Float.equal) t t_n) ;;
      let%test "Proper get_row_at_idx" =
        equal Float.equal
          ({data=get_row_at_idx ~idx:[2] t_n;shape=[2;]})
          ({data=Row [|0.;5.0|];shape=[2;]})
      let%test "Transpose" = Float.equal 5.0 (get_idx t_t [1;2])
    end)
end

module Vertex = struct
  type id = int
  type shape = int list
  let str_shape sh =
    "["^(Int.to_string (hd_id sh))^
    (List.fold_left
       ~f:(fun acc e -> acc^", "^(Int.to_string e))
       ~init:"" (tl_id sh))
    ^ "]"
  let opt_shape_to_str sh = match sh with
    | None -> "No shape"
    | Some s -> str_shape s
  type operator =
    | Add
    | Mul
    | Matmul
    | LogSoftmax
    | ReLu
    | Transpose
    | Squeeze
    | MaxPool
    | Conv
    | Identity
    | NO_OP
    | RW_Linearized_ReLu
  let str_op o = match o with
    | Add -> "Add"
    | Mul -> "Mul"
    | Matmul -> "Matmul"
    | LogSoftmax -> "LogSoftmax"
    | ReLu -> "ReLu"
    | Transpose -> "Transpose"
    | Squeeze -> "Squeeze"
    | MaxPool -> "MaxPool"
    | Conv -> "Conv"
    | Identity -> "Identity"
    | NO_OP -> "NO_OP"
    | RW_Linearized_ReLu -> "RW_Linearized_ReLu"
  let str_shape sh = match sh with
    | [] -> "[]"
    | _ -> "["^(Int.to_string (hd_id sh))^
           (List.fold_left
              ~f:(fun acc e -> acc^", "^(Int.to_string e))
              ~init:"" (tl_id sh))
           ^ "]"
  type ksize = Ksize of shape
  type stride = Stride of shape
  type pads = Pads of shape
  type dilations = Dilations of shape
  type operator_parameters =
    | Pool_params of (ksize*stride option*pads option*dilations option)
    | Conv_params of (ksize*stride option*pads option*dilations option)
    | Transpose_params of shape
    | RW_Linearized_ReLu_params of ((bool list list) * ((string, float) Base.Hashtbl.t list * int))
  let str_op_params p = match p with
    | Transpose_params s ->
      let str_sh = str_shape s
      in "Transpose params: "^str_sh
    | Pool_params (Ksize k,s,p,d) | Conv_params (Ksize k,s,p,d) ->
      let str_k = str_shape k
      and str_s = match s with
        | None -> ""
        | Some Stride ss -> str_shape ss
      and str_p = match p with
        | None -> ""
        | Some Pads pp -> str_shape pp
      and str_d = match d with
        | None -> ""
        | Some Dilations dd -> str_shape dd
      in "Pool params: KSIZE: "^str_k^
         ", Pads: "^str_p^", Stride: "^str_s^
         ", Dilations: "^str_d
    | RW_Linearized_ReLu_params l -> (* Only displays the activation scheme on the ReLU node *)
      let activations = fst l in
      let act' = List.map ~f:(fun l1 -> List.map ~f:(fun b -> match b with true -> "true" | false -> "false") l1) activations in
      let act'' = List.map ~f:(fun l -> "["^(String.concat ~sep:";" l)^"]") act' in
      let act''' = "["^(String.concat ~sep:";" act'')^"]" in
      "RW_Linearized_ReLu_params: "^act'''
  type t = {
    id: id;
    name: string option;
    mutable shape : shape;
    operator: operator;
    operator_parameters: operator_parameters option;
    pred : string list;
    succ : string list;
    tensor : Tensor.t option
  }
  let compare v1 v2 = Stdlib.compare v1.id v2.id
  let hash (v:t) = v.id
  let equal v1 v2 = v1.id = v2.id
  let create ~id ~name ~sh ~op ~op_p ~pred ~succ ~tensor =
    {id=id;name=name;shape=sh;operator=op;
     operator_parameters=op_p;pred=pred;succ=succ;
     tensor=tensor}
  let get_shape t = t.shape
  let get_op t = t.operator
  let get_tensor t = t.tensor
  let is_data_node t = match get_tensor t with
    | None -> false
    | Some _ -> true
  let num_neurons t = match get_shape t with
    | [] -> 0
    | l -> List.fold_left ~init:1 ~f:(fun x acc -> x*acc) l
end

module Edge = struct
  type t = string
  let compare = Stdlib.compare
  let equal = phys_equal
  let default = ""
end

module NierCFG = Graph.Imperative.Digraph.ConcreteBidirectionalLabeled(Vertex)(Edge)
module NierCFGDot = Graph.Graphviz.Dot(struct
    include NierCFG (* use the graph module from above *)
    let node_label (v:vertex) =
      let id = Int.to_string v.id in
      let name = match v.name with
        | Some n -> n
        | None -> "C_NODE"
      and operator = Vertex.str_op v.operator
      and operator_parameters = match v.operator_parameters with
        | Some p -> Vertex.str_op_params p
        | None -> "no parameters"
      and shape = Vertex.str_shape v.shape
      and prevs = List.fold_left
          ~f:(fun x y -> x^","^y) ~init:"" v.pred
      and nexts = List.fold_left
          ~f:(fun x y -> x^","^y) ~init:"" v.succ
      and tensor = match v.tensor with
        (*limit of size for tensor strings, complying with
         * dot string size limit of 16Ko *)
        | Some t -> let display_indices =
                      let all_indices = Tensor.all_coords t.shape in
                      if List.length all_indices > 10 then
                        let rec firstk k xs = match xs with
                          | [] -> failwith "firstk"
                          | x::xs ->
                            if k=1
                            then [x]
                            else x::firstk (k-1) xs
                        in firstk 10 all_indices
                      else all_indices
          in
          let t_value_string =
            List.fold_left
              ~f:(fun acc l ->
                  acc^Vertex.str_shape l^": "^
                  (Float.to_string (Tensor.get_idx t l)^"\n"))
              ~init:"" display_indices
          in
          "Tensor value\n: "^t_value_string^
          "\nShape: "^(Vertex.str_shape t.shape)
        | None -> "No tensor in node"
      in "ID :"^id^"\nNAME: "^name^"\nOP: "^operator^"\nOP PARAMS:"^
         operator_parameters^"\nSHAPE: "^shape^"\nPREVS: "^prevs^
         "\nNEXTS: "^nexts^"\nTENSORS INFOS:"^tensor
    let edge_attributes (_, e, _) = [`Label e; `Color 4711]
    let default_edge_attributes _ = []
    let get_subgraph _ = None
    let vertex_attributes v = [`Shape `Box; `Label (node_label v)]
    let vertex_name (v:vertex) = Int.to_string v.id
    let default_vertex_attributes _ = []
    let graph_attributes _ = []
  end)

(** Helper functions **)
let vertex_list g = NierCFG.fold_vertex (fun x l -> x::l) g []
let input_nodes g =
  let input_criterion (v:Vertex.t) acc = match v.id with
    | 0 -> Some v
    | _ -> acc
  in match NierCFG.fold_vertex (fun v acc -> input_criterion v acc) g None with
  | Some r -> [r]
  | None -> failwith "Something strange, no node for describing inputs found"
let preds g v = NierCFG.pred g v
let preds_names g v =
  let preds_list = NierCFG.pred_e g v in
  List.fold ~init:[] ~f:(fun acc (_,n,_) -> n::acc) preds_list
let succs_names g v =
  let succs_list = NierCFG.succ_e g v in
  List.fold ~init:[] ~f:(fun acc (_,n,_) -> n::acc) succs_list
let succs g v = NierCFG.pred g v
let init_cfg = NierCFG.create ()
(** Find vertices respecting a boolean predicate **)
let find_vertices g f = NierCFG.fold_vertex
    (fun x l -> if f x then x::l else l) g []

(** Some pretty printers *)
let print_cfg_graph g = NierCFGDot.fprint_graph
    Caml.Format.std_formatter g
let out_cfg_graph g =
  let file = Out_channel.create "cfg.dot" in NierCFGDot.output_graph file g
