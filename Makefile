SRCS=*.ml
INTR=*.mli

all:
	dune build @install
static:
	dune build @all
doc:
	dune build @doc
install-deps:
	opam install .
install-ext-deps:
	opam update && opam depext
clean:
	dune clean
