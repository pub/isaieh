(**
 * This module convert an onnx file representing a deep neural network into a
 * SMTLIB2 representation.
 * Main contributors:
 * Zakaria Chihani
 * Julien Girard-Satabin
*)

module Parser = Onnx_parser
module Onnxpiqi = Piqi_interface.Onnx_piqi
module IR = Ir.Nier_cfg
module SMT = Smtlib.Smtifyer
module P = Printf
module Arg = Arg
module F = Filename

exception Nograph of string
let env_cfg = SMT.env_init_cfg
(*inputs and args management*)
let t_cfg input_file theory =
  let model = Parser.parse_model_from_file input_file in
  let graph = match model.Onnxpiqi.Model_proto.graph with
    | Some g -> g
    | None -> raise (Nograph "No graph in ONNX input file!") in
  let nier_cfg = Parser.produce_cfg graph in
  Printf.printf "NIER CFG build successful\n";
  SMT.pp_graph_cfg nier_cfg theory
let theory_v = ref SMT.Real_Theory
let output_dir = ref ""
let set_theory s = match s with
  | "QF_FP"  -> theory_v := SMT.Float_Theory
  | "QF_NRA"  -> theory_v := SMT.Real_Theory
  | "QF_LRA"  -> theory_v := SMT.Linear_Real_Theory
  | _        -> theory_v := SMT.Real_Theory
let theory_to_string = function
  | SMT.Float_Theory -> "QF_FP"
  | SMT.Real_Theory -> "QF_NRA"
  | SMT.Linear_Real_Theory -> "QF_LRA"
let speclist = [
  ("-theory", Arg.Symbol (["QF_NRA";"QF_FP";"QF_LRA"],set_theory),
   "Choose the theory (default is QF_FP)");
  ("-output-dir", Arg.Set_string output_dir,
   "Choose the output directory");
]
let usage_msg = {|Converter from onnx to smt2.
Usage: converter.exe input_file.onnx [OPTIONS].
The input file must be a valid onnx file.
Options are the following:
|}
let input_file_default = ref "."
(*TODO: sanitize input file*)
let process_input_file f = input_file_default := f
let process_output_file i od theory =
  let model_descp = F.chop_extension (F.basename i) in
  od^model_descp^"_"^theory^".smt2"

(* Main function *)
let _ =
  Arg.parse speclist process_input_file usage_msg;
  let input_file = !input_file_default in
  let toprint = t_cfg input_file !theory_v
  in
  (* TODO: error handling *)
  Printf.printf "SMTLIB2 conversion successful\n";
  let output_file = process_output_file input_file !output_dir (theory_to_string !theory_v) in
  let oc = open_out output_file in
  (* List.iter (Printf.printf "%s \n") toprint *)
  List.iter (fun x -> P.fprintf oc "%s\n" x) toprint;
  close_out oc;
  P.printf "Output saved in %s\n" output_file
