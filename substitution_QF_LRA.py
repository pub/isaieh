import sys

def substitution(smt):
    fp = open(smt, 'r')
    to_subst = {}
    lines = []
    to_write = []
    for line in fp:
        lines.append(line)
        if '/' in line:
            if not ('(<' or '(>') in line:
                k = line.split(' ')[2]
                if line.split(' ')[-3] == '(-':
                    v = line.split(' ')[-4:]
                else:
                    v = line.split(' ')[-3:]
                if k=='0':
                    pass  # print("line=", line)
                else:
                    to_subst[k] = v
    keys = to_subst.keys()
    for elt in lines:
        split = elt.split(' ')
        if 'assert' and len(split) == 4:
            k = split[2]
            v = split[3]
            if v.rstrip()[:-2] in keys:
                to_subst[k] = to_subst[v.rstrip()[:-2]]
        for key in keys:
            if key and 'assert' in elt:
                elt = elt.replace(key, ' '.join(to_subst[key]).rstrip()[:-2])
        to_write.append(elt)
    of = open(smt, 'w')
    for x in to_write:
        of.write(x)


if __name__ == '__main__':
    substitution(sys.argv[1])
