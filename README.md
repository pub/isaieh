# Inter-Standard AI Encoding Hub (ISAIEH)

ISAIEH is an undergoing research effort to bridge neural
network and formal verification.

Formal verification of neural network is a blooming field,
numerous tools and technique are already available; surely
more are to come. One issue is that, for now, most of
those tools tend to "reinvent the wheel" regarding
neural network parsing, intermediate representation for
networks simplification, safety property expressions,
and so on.

ISAIEH aims to adress those issues by providing a
standard tooling for neural network verification.
Among those tools are a (primitive) compiler from
neural network ONNX protobuf format to various output formats
adapted to formal proofs, and an intermediate representation
(NIER) to implement network transformations, forward and
backward reasoning.

The roadmap is not fully defined, but among our priorities
are a python API, a verification property formulation engine
, and more output formats (C language is a top priority),
smoother support of ONNX.

Currently, the only available output format is SMTLIB2
logic formulaes description format. The neural
network control flow is compiled to logical formulaes,
ready to be included in a verification process using any
solver supporting SMTLIB2 syntax. Note that for now,
properties to solve must be provided externally.

ONNX support is limited: altough most common operations are
supported (matrix multiplication, ReLUs, maxpooling,
convolutions), only a small subset of ONNX operators is
currently supported.

## Using ISAIEH

ISAIEH is not yet released as a binary, so you will need
to compile it from source.

### Dependencies:
The list of OCaml dependencies is available under the
`dune-project` file at the root of the project.
We require OCaml 4.11.1 at least.
The simplest way to get all dependencies is by using the
OCaml package manager, `opam`,
version 2.0 or higher.
Get it [here](https://opam.ocaml.org/).
Then, run `opam install .` at the root
of project directory, or `make install-deps`.

`opam` will only take care of the OCaml dependencies. For
system dependencies, you may install `depext` using
`opam update && opam install depext`
(or `make install-ext-deps`), then typing
`opam depext` at the root of the project. A list of
necessary dependencies will be displayed and, if
your system is supported, the proper command to install
them will be given.

### Build:
* To build the project after completing the dependencies
installation, type `make` (requires the `make`) utilitary.
* A static binary is available for compilation
(but it requires a statically linked libc,
such as `musl`). To build it, simply type `make static`.
* To build the API documentation, type `make doc`.
It will be available under `_build/default/_doc/_html`.
* To remove the build artifacts, type `make clean`.

### Run:
For now, a basic conversion from onnx to smt2 is the only
available binary.
* To run it, type
`dune exec -- bin/converter/converter.exe /path/to/file.onnx`
at the root of the project.
Output will be saved under `/path/to/file.smt2`.
Change the target theory with option `-theory`.
For instance, to target
QF_NRA theory, type
`dune exec -- bin/converter.exe /path/to/file.onnx -theory QF_NRA`.
Supported theories are QF_NRA, QF_LRA and QF_FP.
For QF_LRA, you will need to pass an additional script on the resulting
SMTLIB2 file `substition_QF_LRA.py`; else the solvers will not be able
to parse correctly the resulting files.

### Tests:
* To run unit tests, type `dune runtest` at root of project.
* To test the printing of graphs, run
`dune exec test/print_graph.exe test/onnx/test_simple.onnx`
A representation of the intermediate representation will
then be outputted as a `.dot` file (using the GraphViz dot
representation format).

Test consists on converting ONNX files to SMT formulaes,
and verifying simple
properties (mostly SAT) on those.
All ONNX files are under the `test` folder.
* `test_simple` is a single layer feedforward network
with a ReLU layer at the end, two neurons wide,
with no biases. Input is of size 2x2.
* `test_wide` is a two layer feedforward network
with 100 neurons width for the first layer and 1 neuron
for the second. Input is of size 500x100.
* `test_linreg_points` is a linear regressor trained to output a pair of parameters (real numbers) $a$ and $b$, given a vector $[a+b,a-b,c]$
* `CAMUS_53` is a network used for the publication _CAMUS_. The property
to verify is in `test/smtlib/property_CAMUS.smt2`. To test ISAIEH on this
problem, execute the script `check_camus.sh`.

## Basic structure of project
ONNX format is parsed into a
Neural IntErmediate Representation (NIER), a
simpler graph representation of the neural network
(most notable difference with
ONNX is merging of architecture and parameters).
If no other instructions are given, NIER data is then
interpreted as an SMT formula respecting SMTLIB2 standards.

### Libraries
All libraries are under the `lib/` folder.
#### onnx_parser
Handles ONNX file parsing and conversion to NIER
#### piqi_interface
Interface to the ONNX specification, embedded as onnx.piqi
#### ir
Definition of NIER and relevant data structures
#### output
Conversion to various output format: currently only
SMTLIB2 is supported

## Publications
ISAIEH is featured in the following publications:
* _CAMUS: A Framework to build formal specifications
for Deep Perception Systems Using Simulators_,
    Julien Girard-Satabin, Guillaume Charpiat,
    Zakaria Chihani, Marc Schoenauer, published at ECAI 2020
* _Partitionnement en régions linéaires pour la
vérification formelle de réseaux de neurones_,
Aymeric Varasse, Julien Girard-Satabin, Guillaume Charpiat,
Zakaria Chihani, Marc Schoenauer, published at J FLA 2021

## Citations
If you found this software useful for your work, please
consider quoting it! A bibtex entry is available here:
```
@software{ISAIEH,
  author = {{Zakaria Chihani, Julien Girard-Satabin}},
  title = {ISAIEH},
  url = {https://git.frama-c.com/pub/isaieh/},
  version = {0.1},
  date = {2020-05-01},
}
```
