echo "Converting a CAMUS ONNX network to SMTLIB2 format."
dune exec -- bin/converter/converter.exe  test/onnx/CAMUS_53.onnx
echo "Adding a property to check."
cat CAMUS_53_QF_NRA.smt2 test/smtlib/property_CAMUS.smt2 > out.smt2
echo "Launching z3. This should take about 15s and return UNSAT."
z3 -st out.smt2

