module Parser = Onnx_parser
module Onnxpq = Piqi_interface.Onnx_piqi
module NCFG = Ir.Nier_cfg
module T = Ir.Nier_cfg.Tensor
module V = Ir.Nier_cfg.Vertex
module N = Ir.Nier_cfg.NierCFG
exception Nograph of string
(* Reads the ONNX model from a file, print graph within*)
let printf = Printf.printf
let unpack x = match x with
  | Some x -> x
  | None -> (T.create [0;])
(* printf "%s" (Sys.getcwd ());; *)
let _ =
  let input_file = "../../../test/onnx/test_simple.onnx" in
  (* let input_file = Sys.argv.(1) in *)
  let model = Parser.parse_model_from_file input_file in
  let graph = match model.Onnxpq.Model_proto.graph with
    | Some g -> g
    | None -> raise (Nograph "No graph in ONNX input file!") in

  let cfg = Parser.produce_cfg graph in
  N.iter_vertex (fun x -> printf "%s\n" (T.show (unpack x.V.tensor))) cfg;
  NCFG.print_cfg_graph cfg; NCFG.out_cfg_graph cfg
