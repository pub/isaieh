#!/bin/bash
solver=$1
smtfile=$2
count=$3
IFS=' '

case ${solver} in
    "z3")
        # command=$(z3 -v:9 -st "${smtfile}" | grep "total-time" | cut -d ' ' -f 11 | cut -d ')' -f 1)
        command=$(z3 -st "${smtfile}" | grep "total-time" | cut -d ' ' -f 11 | cut -d ')' -f 1)
        ;;
    "yices")
        # command=$(yices-smt2 -v 9 -s "${smtfile}" | grep "total-run-time" | cut -d ' ' -f 3)
        command=$(yices-smt2 -s "${smtfile}" | grep "total-run-time" | cut -d ' ' -f 3)
        ;;
    "cvc4")
        command=$(~/Outils/cvc4/cvc4-1.7-x86_64-linux-opt --stats "${smtfile}" 2>&1| tail -n1 | cut -d ' ' -f 2)
        ;;
    # "colibri")
    # unsupported yet
    #     command=$(~/Outils/colibri_2176/colibri "${smtfile}" 2>&1| tail -n1 | cut -d ' ' -f 2)
    #     ;;
    *)
        echo "solver not supported"
        ;;
esac

set -x print command
m=0
for i in {1..2}; do
    echo $command
    val=$command
    m=$(awk "BEGIN {print ${m}+${val}; exit}")
    echo "run finished for $m for solver $solver"
done
set +x stop printing command

m=$(awk "BEGIN {print $m/$i; exit}")
echo "Mean runtime for $i runs for $solver is $m s"
