;;;; We want to ensure that for any obstacle within the danger zone,
;;;; the output corresponding to the CHANGE DIRECTION order will always
;;;; be higher than the NO CHANGE order

;;;; Formulate constraint on inputs:
;; Inputs can be only between 0 and 1
(assert (or (= |CELL_actual_input_0_0_0_0| 0) (= |CELL_actual_input_0_0_0_0| 1)))
(assert (or (= |CELL_actual_input_0_0_0_1| 0) (= |CELL_actual_input_0_0_0_1| 1)))
(assert (or (= |CELL_actual_input_0_0_0_2| 0) (= |CELL_actual_input_0_0_0_2| 1)))
(assert (or (= |CELL_actual_input_0_0_0_3| 0) (= |CELL_actual_input_0_0_0_3| 1)))
(assert (or (= |CELL_actual_input_0_0_0_4| 0) (= |CELL_actual_input_0_0_0_4| 1)))
(assert (or (= |CELL_actual_input_0_0_0_5| 0) (= |CELL_actual_input_0_0_0_5| 1)))
(assert (or (= |CELL_actual_input_0_0_0_6| 0) (= |CELL_actual_input_0_0_0_6| 1)))
(assert (or (= |CELL_actual_input_0_0_0_7| 0) (= |CELL_actual_input_0_0_0_7| 1)))
(assert (or (= |CELL_actual_input_0_0_0_8| 0) (= |CELL_actual_input_0_0_0_8| 1)))
(assert (or (= |CELL_actual_input_0_0_0_9| 0) (= |CELL_actual_input_0_0_0_9| 1)))
(assert (or (= |CELL_actual_input_0_0_0_10| 0) (= |CELL_actual_input_0_0_0_10| 1)))
(assert (or (= |CELL_actual_input_0_0_0_11| 0) (= |CELL_actual_input_0_0_0_11| 1)))
(assert (or (= |CELL_actual_input_0_0_0_12| 0) (= |CELL_actual_input_0_0_0_12| 1)))
(assert (or (= |CELL_actual_input_0_0_0_13| 0) (= |CELL_actual_input_0_0_0_13| 1)))
(assert (or (= |CELL_actual_input_0_0_0_14| 0) (= |CELL_actual_input_0_0_0_14| 1)))
(assert (or (= |CELL_actual_input_0_0_0_15| 0) (= |CELL_actual_input_0_0_0_15| 1)))
(assert (or (= |CELL_actual_input_0_0_0_16| 0) (= |CELL_actual_input_0_0_0_16| 1)))
(assert (or (= |CELL_actual_input_0_0_0_17| 0) (= |CELL_actual_input_0_0_0_17| 1)))
(assert (or (= |CELL_actual_input_0_0_0_18| 0) (= |CELL_actual_input_0_0_0_18| 1)))
(assert (or (= |CELL_actual_input_0_0_0_19| 0) (= |CELL_actual_input_0_0_0_19| 1)))
(assert (or (= |CELL_actual_input_0_0_0_20| 0) (= |CELL_actual_input_0_0_0_20| 1)))
(assert (or (= |CELL_actual_input_0_0_0_21| 0) (= |CELL_actual_input_0_0_0_21| 1)))
(assert (or (= |CELL_actual_input_0_0_0_22| 0) (= |CELL_actual_input_0_0_0_22| 1)))
(assert (or (= |CELL_actual_input_0_0_0_23| 0) (= |CELL_actual_input_0_0_0_23| 1)))
(assert (or (= |CELL_actual_input_0_0_0_24| 0) (= |CELL_actual_input_0_0_0_24| 1)))
;; There is at least one input within the danger zone (pixels from 10)
;; that is equal to 1
(assert (or (= |CELL_actual_input_0_0_0_11| 1)
            (= |CELL_actual_input_0_0_0_12| 1)
            (= |CELL_actual_input_0_0_0_13| 1)
            (= |CELL_actual_input_0_0_0_14| 1)
            (= |CELL_actual_input_0_0_0_15| 1)
            (= |CELL_actual_input_0_0_0_16| 1)
            (= |CELL_actual_input_0_0_0_17| 1)
            (= |CELL_actual_input_0_0_0_18| 1)
            (= |CELL_actual_input_0_0_0_19| 1)
            (= |CELL_actual_input_0_0_0_20| 1)
            (= |CELL_actual_input_0_0_0_21| 1)
            (= |CELL_actual_input_0_0_0_22| 1)
            (= |CELL_actual_input_0_0_0_23| 1)
            (= |CELL_actual_input_0_0_0_24| 1)
            )
)
;; Formulate constraint on outputs:
;; Always fire such that the danger is detected
;; Fire such that no danger is detected
;; (assert (> actual_output_0_0_0_0 actual_output_0_0_0_1))
;; Alternate formulation
;; since we see that outputs are always of different signs,
;; we look that x0 > 0 (since it would mean x1 < 0 thus x0 > x1
(assert (> |CELL_actual_output_0_0_0_0| 0))
;; Only 3 pixels are white at maximum
(assert (> 3
(+ |CELL_actual_input_0_0_0_0| |CELL_actual_input_0_0_0_1|
|CELL_actual_input_0_0_0_2| |CELL_actual_input_0_0_0_3|
|CELL_actual_input_0_0_0_4| |CELL_actual_input_0_0_0_5|
|CELL_actual_input_0_0_0_6| |CELL_actual_input_0_0_0_7|
|CELL_actual_input_0_0_0_8| |CELL_actual_input_0_0_0_9|
|CELL_actual_input_0_0_0_10| |CELL_actual_input_0_0_0_11|
|CELL_actual_input_0_0_0_12| |CELL_actual_input_0_0_0_13|
|CELL_actual_input_0_0_0_14| |CELL_actual_input_0_0_0_15|
|CELL_actual_input_0_0_0_16| |CELL_actual_input_0_0_0_17|
|CELL_actual_input_0_0_0_18| |CELL_actual_input_0_0_0_19|
|CELL_actual_input_0_0_0_20| |CELL_actual_input_0_0_0_21|
|CELL_actual_input_0_0_0_22| |CELL_actual_input_0_0_0_23|
|CELL_actual_input_0_0_0_24|
)))
;; Check satisfiability
(check-sat)
(get-value (|CELL_actual_input_0_0_0_0|))
(get-value (|CELL_actual_input_0_0_0_1|))
(get-value (|CELL_actual_input_0_0_0_2|))
(get-value (|CELL_actual_input_0_0_0_3|))
(get-value (|CELL_actual_input_0_0_0_4|))
(get-value (|CELL_actual_input_0_0_0_5|))
(get-value (|CELL_actual_input_0_0_0_6|))
(get-value (|CELL_actual_input_0_0_0_7|))
(get-value (|CELL_actual_input_0_0_0_8|))
(get-value (|CELL_actual_input_0_0_0_9|))
(get-value (|CELL_actual_input_0_0_0_10|))
(get-value (|CELL_actual_input_0_0_0_11|))
(get-value (|CELL_actual_input_0_0_0_12|))
(get-value (|CELL_actual_input_0_0_0_13|))
(get-value (|CELL_actual_input_0_0_0_14|))
(get-value (|CELL_actual_input_0_0_0_15|))
(get-value (|CELL_actual_input_0_0_0_16|))
(get-value (|CELL_actual_input_0_0_0_17|))
(get-value (|CELL_actual_input_0_0_0_18|))
(get-value (|CELL_actual_input_0_0_0_19|))
(get-value (|CELL_actual_input_0_0_0_20|))
(get-value (|CELL_actual_input_0_0_0_21|))
(get-value (|CELL_actual_input_0_0_0_22|))
(get-value (|CELL_actual_input_0_0_0_23|))
(get-value (|CELL_actual_input_0_0_0_24|))
